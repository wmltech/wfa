from django import forms
from uaa.models import User
from teams.models import Team,TeamMember
from .models import Project, Section,Category,Severity,Task
from django.utils import timezone


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        exclude = ['status', 'created_by', 'modified_by', 'slug']
        # widgets = {'organisation': forms.HiddenInput()}


class ProjectSectionForm(forms.ModelForm):
    class Meta:
        model = Section
        exclude = ['order', 'created_by', 'modified_by','project']

class DeleteSectionForm(forms.Form):
    confirm = forms.BooleanField(required=True, label='Are you sure you want to delete this section.If you delete this section all tasks related to this section also delete.')

class DeleteProjectForm(forms.Form):
    confirm = forms.BooleanField(required=True, label='Are you sure you want to delete this project.If you delete this project all sections,tasks related to this project also delete.')

class DeleteTaskForm(forms.Form):
    confirm = forms.BooleanField(required=True, label='Are you sure you want to delete this task.')


class ProjectTaskForm(forms.ModelForm):
    class Meta:
        model = Task
        exclude = ['assigned_to_team',    'assigned_to_user', 'assigned_by', 'assigned_time', 'created_by', 'modified_by',   ]
        #widgets = {
        #    'start_date': forms.DateTimeInput(),
        #    'end_date': forms.DateInput(),
        #}

class EditParentTaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['name','reference','section']

class EditGenricApprovalForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = ['name','reference','section','parent', 'dependent_on_section', 'dependent_on_task','duration','target_start_datetime','target_end_datetime','assigned_to_team', 'assigned_to_user']

"""
class TaskForm(forms.Form):
    name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'form-control'}))
    section = forms.ChoiceField(widget=forms.Select(attrs={'class': 'select form-control'}))
    dependent_on = forms.ChoiceField(widget=forms.Select(attrs={'class': 'select form-control'}), required=False)
    start_datetime = forms.DateTimeField(label='Start Time', widget=forms.TextInput(attrs={'class': 'form-control'}))
    end_datetime = forms.DateTimeField(label='End Time', widget=forms.TextInput(attrs={'class': 'form-control'}))
"""

class TaskApprovalForm(forms.Form):
    name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'form-control'}))
    #assign_to_team = forms.ChoiceField(choices = [('', '--------'),] + [(x.id, x.name) for x in Team.objects.all()], widget=forms.Select(attrs={'class': 'select form-control assign_to_team'}))
    #assign_to_user = forms.ChoiceField(choices = [('', '--------'),] + [(x.id, x.get_full_name()) for x in User.objects.all()],widget=forms.Select(attrs={'class': 'select form-control assign_to_user'}))


class TeamModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.name


class UserModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.get_full_name()


class TaskModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return '{} - {}'.format(obj.section.name, obj.name)


class DependentSectionChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.name

class DependentTaskModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        if obj.parent:
            return '{} - {}'.format(obj.parent.name, obj.name)
        else:
            return obj.name



class TaskForm(forms.Form):

    name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class': 'form-control'}))
    reference = forms.CharField(max_length=50, required=False,widget=forms.TextInput(attrs={'class': 'form-control'}))

    def __init__(self, *args, **kwargs):
        self.project = kwargs.pop('project')
        self.task_type = kwargs.pop('task_type')
        self.parent = kwargs.pop('parent')
        self.instance = None
        if 'instance' in kwargs:
            self.instance = kwargs.pop('instance')
        super(TaskForm, self).__init__(*args, **kwargs)

        if not self.parent:
            self.fields['section'] = forms.CharField(widget=forms.Select(attrs={'class': 'select form-control'}))
            self.fields['category'] = forms.CharField(widget=forms.Select(attrs={'class': 'select form-control'}), required=False)
            self.fields['severity'] = forms.CharField(widget=forms.Select(attrs={'class': 'select form-control'}), required=False)

        if self.instance:
            self.fields['name'].initial = self.instance.name
            self.fields['reference'].initial = self.instance.reference

        if self.task_type in [ 'approval', 'generic' ]:

            if not self.parent:

                self.fields['parent'] = TaskModelChoiceField(
                    widget=forms.Select(attrs={'class': 'select form-control'}),
                    queryset= Task.objects.filter(section__project=self.project, parent__isnull=True, type='P'),
                    required=False
                )

            self.fields['dependent_on_section'] = DependentSectionChoiceField(
                widget=forms.Select(attrs={'class': 'select form-control'}),
                queryset=Section.objects.filter(project=self.project).distinct(),
                required=False)

            self.fields['dependent_on_task'] = DependentTaskModelChoiceField(
                widget=forms.Select(attrs={'class': 'select form-control', 'disabled': True}, ),
                queryset=Task.objects.filter(section__project=self.project).exclude(type='P').order_by('section','id'),
                required=False)



            """
            self.fields['dependent_on'] = forms.CharField(widget=forms.Select(attrs={'class': 'select form-control', 'multiple': 'multiple'}),
                                                      required=False)
            
            
            self.fields['dependent_on'] = TaskModelChoiceField(
                widget=forms.SelectMultiple(attrs={'class': 'select form-control'}),
                queryset=Task.objects.filter(section__project=self.project).exclude(type='P').order_by('section','id'),
                required=False,
            )
            
            DEPENDENT_CHOICE= Task.objects.filter(section__project=self.project).exclude(type='P').order_by('section', 'id')
            self.fields['dependent_on'] = forms.MultipleChoiceField(
                required=False,
                widget=forms.SelectMultiple(),
                choices=[(x,x.name) for x in DEPENDENT_CHOICE]
            )
            """
            self.fields['duration'] = forms.IntegerField(
                label='Duration',
                initial=3,
                widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Duration in days'}),
                required=False,
                help_text = 'Days within which this task needs to be completed'
            )
            self.fields['start_datetime'] = forms.DateTimeField(
                label='Start Time',
                widget=forms.TextInput(attrs={'class': 'form-control'}),
                required=False
            )
            self.fields['end_datetime'] = forms.DateTimeField(
                label='End Time',
                widget=forms.TextInput(attrs={'class': 'form-control',}),
                required=False
            )

            self.fields['assign_to_team'] = TeamModelChoiceField(
                widget=forms.Select(attrs={'class': 'select form-control assign_to_team'}),
                queryset=Team.objects.filter(organisation = self.project.organisation,is_system=False),
                required=False
            )
            self.fields['assign_to_user'] = UserModelChoiceField(
                widget=forms.Select(attrs={'class': 'select form-control assign_to_team', 'disabled': True}),
                queryset=User.objects.filter(teammember__team__organisation=self.project.organisation, is_active= True).distinct(),
                required=False
            )

            if self.instance:
                #self.fields['name'].initial = self.instance.name
                #self.fields['reference'].initial = self.instance.reference
                #self.fields['section'].initial = self.instance.section.id
                #self.fields['parent'].initial = self.instance.parent
                #self.fields['dependent_on'].initial = self.instance.dependent_on
                self.fields['duration'].initial = self.instance.duration
                self.fields['start_datetime'].initial = self.instance.target_start_datetime
                self.fields['end_datetime'].initial = self.instance.target_end_datetime
                self.fields['assign_to_team'].initial = self.instance.assigned_to_team
                self.fields['assign_to_user'].initial = self.instance.assigned_to_user



    def clean_start_datetime(self):
        dependent_on_section = self.cleaned_data.get('dependent_on_section')
        # dependent_on_task = self.cleaned_data.get('dependent_on_task')
        start_datetime = self.cleaned_data.get('start_datetime', None)

        if not start_datetime and not dependent_on_section:
            raise forms.ValidationError('Please provide start date time')

        return start_datetime

    def clean_end_datetime(self):
        dependent_on_section = self.cleaned_data.get('dependent_on_section')
        start_datetime = self.cleaned_data.get('start_datetime', None)
        end_datetime = self.cleaned_data.get('end_datetime', None)

        if start_datetime and not end_datetime:
            raise forms.ValidationError('Please provide end date time')

        if start_datetime and end_datetime and (start_datetime > end_datetime):
            raise forms.ValidationError('End datetime cannot be older than start datetime')

        if not start_datetime and not end_datetime and not dependent_on_section:
            raise forms.ValidationError('Please provide start date time and end date time')

        return end_datetime

    def clean_duration(self):
        dependent_on_section = self.cleaned_data.get('dependent_on_section')
        # start_datetime = self.cleaned_data['start_datetime']
        duration = self.cleaned_data.get('duration', None)

        # end_datetime = self.cleaned_data['end_datetime']

        if dependent_on_section and not duration:
            raise forms.ValidationError('Please provide duration')

        return duration


    def clean_assign_to_user(self):

        assign_to_team = self.cleaned_data.get('assign_to_team', None)
        assign_to_user = self.cleaned_data.get('assign_to_user', None)

        if assign_to_team and assign_to_user:
            try:
                TeamMember.objects.get(team=assign_to_team, member=assign_to_user)
            except TeamMember.DoesNotExist:
                raise forms.ValidationError('selected assign to user is not in selected team')

        if not assign_to_team and not assign_to_user:
            raise forms.ValidationError('Please Provide Assign to Team Or Assign To User')

        return assign_to_user

    """
    def clean_assign_to_team(self):

        assign_to_team = self.cleaned_data.get('assign_to_team', None)
        assign_to_user = self.cleaned_data.get('assign_to_user', None)

        if not assign_to_team and not assign_to_user:
            raise forms.ValidationError('Please Provide Assign to Team')
        return assign_to_team

    """

    def save(self, user):
        data = self.cleaned_data
        print('DATA: ', data)
        name = data.get('name')

        reference = data.get('reference')
        # dependent_on = data.getlist('dependent_on')
        dependent_on_section = data.get('dependent_on_section')
        dependent_on_task = data.get('dependent_on_task')
        duration = data.get('duration')
        target_start_date = data.get('start_datetime')
        target_end_date = data.get('end_datetime')
        assign_to_team = data.get('assign_to_team')
        assign_to_user = data.get('assign_to_user')

        if self.parent:
            parent = self.parent
            section = parent.section.id
            if parent.category:
                category = parent.category.id
            else:
                category = None
            if parent.severity:
                severity = parent.severity.id
            else:
                severity = None
        else:
            parent = data.get('parent')
            section = data.get('section')
            category = data.get('category')
            severity = data.get('severity')
        try:
            section_id = int(section)
            section_obj = Section.objects.get(id=section_id)
        except Exception:
            try:
                section_obj = Section.objects.get(name=section, project=self.project)
            except Section.DoesNotExist:
                order = Section.objects.filter(project=self.project).count() + 1
                section_obj = Section.objects.create(name=section, project=self.project, created_by=user,
                                                     modified_by=user, order=order)
        category_obj = None
        try:
            category_id = int(category)
            category_obj = Category.objects.get(id=category_id)
        except Exception:
            try:
                category_obj = Category.objects.get(name=category, project=self.project)
            except Category.DoesNotExist:
                order = Category.objects.filter(project=self.project).count() + 1
                if category:
                    category_obj = Category.objects.create(name=category, project=self.project, created_by=user,
                                                     modified_by=user, order=order)
        severity_obj = None
        try:
            severity_id = int(severity)
            severity_obj = Severity.objects.get(id=severity_id)
        except Exception:
            try:
                severity_obj = Severity.objects.get(name=severity,project=self.project)
            except Severity.DoesNotExist:
                order = Severity.objects.filter(project=self.project).count() + 1
                if severity:
                    severity_obj = Severity.objects.create(name=severity, project=self.project, created_by=user,
                                                     modified_by=user, order=order)




        if parent:
            order = Task.objects.filter(parent=parent).count() + 1
        else:
            order = Task.objects.filter(section=section_obj).count() + 1


        if self.task_type == 'approval':

            if self.instance:
                print('HERE')
                task = self.instance
                task.name = name
                task.reference = reference
                #task.section= section
                #task.category= category
                #task.severity =severity
                #task.parent = parent
                # task.dependent_on = dependent_on
                task.duration = duration
                task.target_start_datetime =target_start_date
                task.target_end_datetime = target_end_date
                task.assigned_to_team = assign_to_team
                task.assigned_to_user = assign_to_user
                task.modified_by = user
                task.save()
            else:
                status = 'PENDING'
                if target_start_date and target_start_date < timezone.now():
                    status = 'STARTED'
                task = Task.objects.create(
                    name=name, section=section_obj,category=category_obj ,severity=severity_obj, reference=reference,
                    status=status, type='A', parent=parent, dependent_on_section=dependent_on_section,
                    dependent_on_task=dependent_on_task, target_start_datetime=target_start_date,
                    target_end_datetime=target_end_date, assigned_to_team=assign_to_team,
                    assigned_to_user=assign_to_user, created_by=user, modified_by=user,order=order
                )
            if parent and task.status == 'STARTED':
                parent.status = task.status
                parent.save()

            return task

        elif self.task_type == 'generic':

            if self.instance:
                task = self.instance
                task.name = name
                task.reference = reference
                #task.section= section
                #task.category= category
                # task.severity =severity
                #task.parent = parent
                task.dependent_on_section = dependent_on_section
                task.dependent_on_task = dependent_on_task
                task.duration = duration
                task.target_start_datetime =target_start_date
                task.target_end_datetime = target_end_date
                task.assigned_to_team = assign_to_team
                task.assigned_to_user = assign_to_user
                task.modified_by = user
                task.save()
            else:
                status = 'PENDING'
                if target_start_date and target_start_date < timezone.now():
                    status = 'STARTED'
                task = Task.objects.create(
                    name=name, section=section_obj,category=category_obj,severity=severity_obj, reference=reference,
                    status=status, type='G', parent=parent, dependent_on_section=dependent_on_section,
                    dependent_on_task=dependent_on_task, duration=duration, target_start_datetime=target_start_date,
                    target_end_datetime=target_end_date,
                    assigned_to_team=assign_to_team, assigned_to_user=assign_to_user, created_by=user, modified_by=user,
                    order=order
            )

            if parent and task.status == 'STARTED':
                parent.status = task.status
                parent.save()
            return task

        elif self.task_type == 'parent':

            if self.instance:
                print('HERE')
                task = self.instance
                task.name = name
                task.reference = reference
                task.status = self.instance.status
                #task.section= section
                #task.category= category
                # task.severity =severity
                #task.parent = parent
                task.modified_by = user
                task.save()
            else:

                task = Task.objects.create(
                    name=name, section=section_obj,category=category_obj,severity=severity_obj,reference=reference, status='PENDING', type='P',
                    created_by=user, modified_by=user,
                    order=order
                )
            return task

    def clone(self, user):

        data = self.cleaned_data

        name = data.get('name')
        reference = data.get('reference')
        section = data.get('section')
        category = data.get('category')
        severity = data.get('severity')
        parent = data.get('parent')
        dependent_on = data.get('dependent_on')
        duration = data.get('duration')
        target_start_date = data.get('start_datetime')
        target_end_date = data.get('end_datetime')

        assign_to_team = data.get('assign_to_team')
        assign_to_user = data.get('assign_to_user')
        try:
            category_id = int(category)
            category = Category.objects.get(id=category_id)
        except Exception:
            try:
                category = Category.objects.get(name=category, project=self.project)
            except Category.DoesNotExist:
                order = Category.objects.filter(project=self.project).count() + 1
        try:
            severity_id = int(severity)
            severity =  Severity.objects.get(id=severity_id)
        except Exception:
            try:
                severity = Severity.objects.get(name=severity,project=self.project)
            except Severity.DoesNotExist:
                order = Severity.objects.filter(project=self.project).count() + 1


        if self.instance:
            task = self.instance
            status = 'PENDING'
            if target_start_date and target_start_date < timezone.now():
                status = 'STARTED'
            section = task.section
            parent = task.parent
            type = task.type
            task.modified_by = user

            task = Task.objects.create(
                name=name, section=section, category=category,severity=severity, reference=reference, type=type,status=status,
                parent=parent,
                dependent_on=dependent_on, duration=duration, target_start_datetime=target_start_date,
                target_end_datetime=target_end_date,
                assigned_to_team=assign_to_team, assigned_to_user=assign_to_user, created_by=user, modified_by=user
            )
            return task





