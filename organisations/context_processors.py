from .models import Organisation
from teams.models import Team
from projects.models import Project


def organisations(request):
    """

    :param request:
    :return:
    This should return organisations that user is member of
    """
    ctx_organisation = None
    ctx_organisations = Organisation.objects.none()
    ctx_projects = Project.objects.none()
    ctx_teams = Team.objects.none()

    if request.user.is_authenticated:
        ctx_organisations = Organisation.objects.filter(team__teammember__member=request.user).distinct()
        if ctx_organisations:
            try:
                ctx_organisation = ctx_organisations.get(slug=request.session.get('U_ORGANISATION'))
            except Organisation.DoesNotExist as e:
                ctx_organisation = ctx_organisations.first()
            request.session['U_ORGANISATION'] = ctx_organisation.slug
            ctx_projects = Project.objects.filter(organisation=ctx_organisation).select_related()
            ctx_teams = Team.objects.filter(organisation=ctx_organisation,is_active = True, is_system=False).select_related().order_by('name')

    return {
        'ctx_organisation': ctx_organisation, 'ctx_organisations': ctx_organisations,  'ctx_projects': ctx_projects,
        'ctx_teams': ctx_teams
    }
