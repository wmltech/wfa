from teams.models import TeamMember
from .models import Project,Task
from django.utils import timezone
from django.template.loader import get_template
from django.core.mail import EmailMessage
from django.conf import settings
from uaa.helpers import get_base_url


def complete_task(task_id):
    task = Task.objects.get(pk=task_id)
    task.status = 'SUCCESS'
    task.save()

    for dependent_task in Task.objects.filter(dependent_on=task):
        if dependent_task.target_start_datetime:
            if dependent_task.target_start_datetime <= timezone.now():
                dependent_task.status = 'STARTED'
                dependent_task.save()
        else:
            dependent_task.status = 'STARTED'
            dependent_task.save()


def update_project_task_status(project_id):

    Task.objects.filter(dependent_on__isnull=True, status='PENDING', target_start_datetime__lte=timezone.now(), section__project__id=project_id).exclude(type='P').update(status='STARTED')

    tasks = Task.objects.filter(dependent_on__isnull=False, status='PENDING', section__project__id=project_id).exclude(type='P')
    for task in tasks:
        if task.dependent_on.status == 'SUCCESS':

            if task.target_start_datetime:
                if task.target_start_datetime <= timezone.now():
                    task.status = 'STARTED'
                    task.save()
            else:
                task.status = 'STARTED'
                task.save()

def update_project_status(project_id):
    project = Project.objects.get(id=project_id)
    tasks = Task.objects.filter(section__project=project).exclude(type='P')
    for task in tasks:
        if task.status =='SUCCESS' and task.status =='FAILURE' and task.satus != 'STARTED':
            project.status='COMPLETED'
            project.save()
        elif task.status =='STARTED' and task.status !='SUCCESS' and task.status !='FAILURE':
            project.status = 'STARTED'
            project.save()


def email_task_owners(id, base_url='http://localhost:8000'):
    task = Task.objects.get(id=id)

    #if task.status == 'STARTED':
    owners = []
    if task.assigned_to_user:
        owners.append(task.assigned_to_user.email)
    elif task.assigned_to_team:
        for member in TeamMember.objects.filter(team=task.assigned_to_team):
            owners.append(member.member.email)
    subject = 'Task Action'
    ctx = {
        'task': task,
        'base_url': base_url
    }

    message = get_template('projects/emails/task_action_email.html').render(ctx)
    msg = EmailMessage(subject, message, to=owners, from_email=settings.DEFAULT_FROM_EMAIL)
    msg.content_subtype = 'html'
    msg.send()
