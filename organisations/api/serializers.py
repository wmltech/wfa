from django.utils import timezone
from rest_framework import serializers
from rest_framework.fields import CurrentUserDefault
from organisations.models import Organisation
from uaa.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id','username',)

class OrganisationSerializer(serializers.ModelSerializer):
    created_by = UserSerializer()
    modified_by = UserSerializer()
    deleted_by = UserSerializer()
    class Meta:
        model = Organisation
        fields = ('url', 'id', 'name', 'slug', 'created_by', 'modified_by', 'deleted_by')
        read_only_fields = ('slug', 'created_by', 'modified_by', 'deleted_by')

    def create(self, validated_data):
        user = self.context['request'].user
        obj = Organisation.objects.create(created_by=user, modified_by=user, **validated_data)
        return obj


    def destroy(self, validated_data):
        user = self.context['request'].user
        print(self)
        print(validated_data)
        obj = Organisation.objects.delete(deleted_by=user, deleted=timezone.now(), **validated_data)
        print("HERE")
        return obj