from django.contrib import admin
from .models import Organisation


# Register your models here.
@admin.register(Organisation)
class OrganisationAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'active', 'created', 'created_by', 'modified', 'modified_by')

