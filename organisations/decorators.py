from django.core.exceptions import PermissionDenied
from .models import Organisation
from teams.models import Team


def user_is_organisation_member(function):
    def wrap(request, *args, **kwargs):
        slug = kwargs.get('slug', None)
        if not Organisation.objects.filter(slug=slug, team__teammember__member=request.user, active=True):
            raise PermissionDenied
        return function(request, *args, **kwargs)
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap
