from django.contrib import admin
from .models import Project, Section,Category,Severity, Task, Note, Attachment


# Register your models here.
@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'organisation', 'created', 'created_by', 'modified', 'modified_by')
    list_filter = ('organisation',)


@admin.register(Section)
class SectionAdmin(admin.ModelAdmin):
    list_display = ('name', 'project', 'order')
    list_filter = ('project',)

@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'project', 'order')
    list_filter = ('project',)

@admin.register(Severity)
class SeverityAdmin(admin.ModelAdmin):
    list_display = ('name', 'project', 'order')
    list_filter = ('project',)



@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ('name', 'section', 'type', 'status', 'order', 'parent')
    list_filter = ('section__project', 'section',)


@admin.register(Note)
class NoteAdmin(admin.ModelAdmin):
    list_display = ('id', 'task', 'created_by', 'modified_by')


@admin.register(Attachment)
class NoteAdmin(admin.ModelAdmin):
    list_display = ('id', 'task', 'name', 'file', 'description', 'created_by', 'modified_by')