from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework import generics
from projects.models import Project, Task, Attachment
from rest_framework.response import Response
from rest_framework import status
from .serializers import (
    SectionOrderSerializer,
    TaskOrderSerializer,
    TaskApprovalSerializer,
    TaskCompleteSerializer,
    TaskNoteSerializer,
    TaskAttachmentSerializer,
    TaskChoiceSerializer,
)
from rest_framework.parsers import FileUploadParser, MultiPartParser, JSONParser, FormParser


class SectionOrderView(generics.GenericAPIView):
    serializer_class = SectionOrderSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({}, status=status.HTTP_201_CREATED)


class TaskOrderView(generics.GenericAPIView):
    serializer_class = TaskOrderSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({}, status=status.HTTP_201_CREATED)


class TaskApprovalView(generics.GenericAPIView):
    serializer_class = TaskApprovalSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        print(request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({}, status=status.HTTP_201_CREATED)


class TaskCompleteView(generics.GenericAPIView):
    serializer_class = TaskCompleteSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({}, status=status.HTTP_201_CREATED)


class TaskNoteView(generics.GenericAPIView):
    serializer_class = TaskNoteSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({}, status=status.HTTP_201_CREATED)


class TaskAttachmentView(generics.GenericAPIView):
    serializer_class = TaskAttachmentSerializer

    # parser_classes = (FormParser, MultiPartParser, FileUploadParser)
    parser_classes = (MultiPartParser,)

    def post(self, request, *args, **kwargs):
        print(request.data)
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        file = request.data['file']
        task_id = request.data.get('task')
        task = Task.objects.get(pk=task_id)
        name = request.data.get('name')
        description = request.data.get('description')

        Attachment.objects.create(
            task=task, file=file, name=name, description=description, created_by=request.user, modified_by=request.user
        )

        #serializer.save()
        return Response({}, status=status.HTTP_201_CREATED)


class TaskChoiceView(generics.ListAPIView):
    serializer_class = TaskChoiceSerializer

    def get_queryset(self,):
        section_id = self.request.GET.get('section_id', None)
        project_id = self.request.GET.get('project_id', None)
        filters = {'section_id': section_id, 'section__project_id': project_id}

        query_set = Task.objects.filter(**filters).exclude(type='P').select_related()
        return query_set
