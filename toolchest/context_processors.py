from django.conf import settings


def get_base_url(request):
    if hasattr(settings, 'BASE_URL'):
        base_url = settings.BASE_URL
    else:
        base_url = '{}://{}'.format(request.scheme, request.get_host())

    return {'ctx_base_url': base_url}