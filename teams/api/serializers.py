from rest_framework import serializers
from django.utils import timezone
from organisations.models import Organisation
from teams.models import Team,TeamMember
from uaa.models import User
from uaa.helpers import get_base_url, send_user_creation_email
from django.template.loader import render_to_string, get_template
from django.core.mail import EmailMessage
from django.conf import settings

def get_base_url(request):
    """
    :param request: Takes HTTP request as input to evaluate base url
    :return: Site URL (base) either from settings or from request.
    """
    if hasattr(settings, 'BASE_URL'):
        base_url = settings.BASE_URL
    else:
        base_url = '{}://{}'.format(request.scheme, request.get_host())
    return base_url


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id','username', )

class OrganisationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Organisation
        fields = ('id','name',)

class TeamSerializer(serializers.ModelSerializer):

    class Meta:
        model = Team
        fields = ('id','name',)

class TeamsSerializer(serializers.ModelSerializer):

    organisation =  OrganisationSerializer()
    created_by = UserSerializer()
    modified_by = UserSerializer()
    deleted_by = UserSerializer()

    class Meta:
        model = Team
        fields = ('url', 'id', 'name', 'slug', 'organisation', 'created_by', 'modified_by', 'deleted_by')
        read_only_fields = ('slug', 'created_by', 'modified_by', 'deleted_by')

    def create(self, validated_data):
        user = self.context['request'].user
        obj = Team.objects.create(created_by=user, modified_by=user, **validated_data)
        return obj

    def delete(self, validated_data):
        user = self.context['request'].user
        obj = Team.objects.delete(deleted_by=user, deleted=timezone.now(), **validated_data)
        return obj



class TeamMembersSerializer(serializers.ModelSerializer):

    member = UserSerializer()
    team =  TeamSerializer()

    class Meta:
        model = TeamMember
        fields = ('url', 'id', 'admin', 'active', 'member', 'team')

    def create(self, validated_data):
        user = self.context['request'].user
        obj = Team.objects.create(created_by=user, joined=timezone.now(), **validated_data)
        return obj


class UserSerializerWithFullName(serializers.Serializer):
    id = serializers.IntegerField()
    username = serializers.CharField()
    email = serializers.EmailField()
    full_name = serializers.CharField()


class TeamMemberSerialiser(serializers.Serializer):
    members = serializers.ListField(child=serializers.CharField(),min_length=1)

    def validate_members(self, values):
        print(values)
        for value in values:
            try:
                value = int(value)
                try:
                    User.objects.get(id=value)
                except User.DoesNotExist:
                    raise serializers.ValidationError(["Invalid user, Please enter valid email address"])
            except ValueError:
                from django.core.validators import validate_email
                try:
                    validate_email(value)
                except:
                    raise serializers.ValidationError(["Please enter valid email address"])
        return values

    def add(self, request, organisation_id, team_id):
        organisation = Organisation.objects.get(id=organisation_id)
        team = Team.objects.get(id=team_id, organisation=organisation)
        members = self.data.get('members')
        user_created = False
        print(self.data, 'SELF.DATA')
        print(members, 'add mem')
        for member in members:
            try:
                member = int(member)
                print(member)
                user = User.objects.get(id=member)
            except ValueError:
                try:
                    user = User.objects.get(email=member)
                except User.DoesNotExist:
                    user = User.objects.create_user(username=member, email=member, password=None)
                    user.is_active = False
                    user.save()
                    send_user_creation_email(user, get_base_url(request), team=team)
                    user_created = True

            team_member, created = TeamMember.objects.get_or_create(team=team, member=user)
            if not user_created:
                subject = 'Team Invitation'
                ctx = {
                    'name': user.first_name,
                    'team': team,
                    'organisation': organisation,
                    'base_url':get_base_url(request)
                }

                message = get_template('teams/team_invitation_email.html').render(ctx)
                user.email_user(subject=subject, message=message, html_message=message)

        return user

    def remove(self,request, organisation_id, team_id):
        organisation = Organisation.objects.get(id=organisation_id)
        team = Team.objects.get(id=team_id, organisation=organisation)
        print(self.data, 'SELF.DATA')
        members = self.data.get("members")
        for member in members:

            member = int(member)

            user = User.objects.get(id=member)
            subject = 'Remove From Team'
            ctx = {
                'name': user.first_name,
                'team': team,
                'base_url': get_base_url(request)
            }

            message = get_template('teams/team_remove_email.html').render(ctx)
            user.email_user(subject=subject, message=message, html_message=message)

            TeamMember.objects.filter(team=team, member=user).delete()
        """
        teamadmin = TeamMember.objects.filter(team=team, member=request.user, admin=True) | TeamMember.objects.filter(
            organisation_team_teammember_member=request.user, team__name__in=('Administrators', 'Managers'))
        print(teamadmin)
        if teamadmin:
            TeamMember.objects.filter(team=team, member=user).delete()
        else:
            raise ValidationError('You do not have permissions to delete team')
        """
class MakeAdminTeamMemberSerialiser(serializers.Serializer):
    organisation = serializers.IntegerField()
    team = serializers.IntegerField()
    member = serializers.ListField(child=serializers.IntegerField())



    def make_admin(self):
        organisation_id = self.data.get('organisation')
        team_id = self.data.get('team')
        member_ids = self.data.get('member')
        for member_id in member_ids:
            TeamMember.objects.filter(team__organisation__id=organisation_id, team__id=team_id, member__id=member_id).update(admin=True)


class RemoveAdminTeamMemberSerialiser(serializers.Serializer):
    organisation = serializers.IntegerField()
    team = serializers.IntegerField()
    member = serializers.ListField(child=serializers.IntegerField())


    def remove_admin(self):
        organisation_id = self.data.get('organisation')
        team_id = self.data.get('team')
        member_ids = self.data.get('member')
        print(member_ids)
        for member_id in member_ids:
            TeamMember.objects.filter(team__organisation__id=organisation_id, team__id=team_id, member__id=member_id).update(admin=False)


class TeamMemberChoiceSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    def get_name(self, obj):
        return obj.get_full_name()



    class Meta:
        model = User
        fields = ('id', 'username', 'name')