from django.conf import settings
from django.db import models
from toolchest.slugger import unique_slugify



# Create your models here.
class Organisation(models.Model):
    name = models.CharField(max_length=60, unique=True)
    slug = models.SlugField(max_length=60, blank=True)
    active = models.BooleanField(default=True)
    project_prefix = models.CharField(max_length=10, default='PR')
    task_prefix = models.CharField(max_length=10, default='TSK')

    # Additional Info
    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='organisation_created_by', on_delete=models.CASCADE)
    modified = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='organisation_modified_by', on_delete=models.CASCADE)
    deleted = models.DateTimeField(blank=True, null=True)
    deleted_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='organisation_deleted_by', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name

    def save(self, **kwargs):
        """Override save method to implement unique slug"""
        if not self.slug:
            unique_slugify(self, self.name)
        else:
            unique_slugify(self, self.slug)
        super(Organisation, self).save()

