from django.urls import path
from django.contrib.auth import views as auth_views

from . import views

urlpatterns = [
    # Registration URLs
    path('register/', views.register, name='uaa_register'),
    path('register/complete/', views.register_complete, name='uaa_register_complete'),
    path('edit-profile/', views.edit_profile, name='uaa_edit_profile'),
    path('activate/<activation_key>/', views.activate, name='uaa_activate'),
    path('activate/complete', views.activate_complete, name='uaa_activate_complete'),

    # Login and Logout URLs
    path('login/', auth_views.LoginView.as_view(template_name='uaa/login.html'), name='uaa_login'),
    path('logout/', auth_views.LogoutView.as_view(), name='uaa_logout'),

    path('password_change/', auth_views.PasswordChangeView.as_view(template_name='uaa/password_change.html'), name='password_change'),
    path('password_change/done/', auth_views.PasswordChangeDoneView.as_view(template_name='uaa/password_change_done.html'), name='password_change_done'),

    path('password_reset/', auth_views.PasswordResetView.as_view(template_name='uaa/password_reset.html', html_email_template_name = 'uaa/emails/password_reset_email.html',subject_template_name ='uaa/emails/password_reset_subject.txt'), name='password_reset'),
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(template_name='uaa/password_reset_done.html'), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name='uaa/password_reset_confirm.html'), name='password_reset_confirm'),
    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(template_name='uaa/password_reset_complete.html'), name='password_reset_complete'),
]
