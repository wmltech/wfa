from django.urls import path, include
from rest_framework import routers
from teams.api.views import TeamViewSet,TeamMemberViewSet, AddTeamMemberView, RemoveTeamMemberView,MakeAdminTeamMemberView,RemoveAdminTeamMemberView, TeamUserView
from projects.api.views import SectionOrderView,TaskOrderView, TaskApprovalView, TaskCompleteView, TaskNoteView, TaskAttachmentView, TaskChoiceView
from organisations.api import views as organisations_api_views
from organisations.api.views import  OrganisationMembersView
from toolchest.api.views import ContactUsView
router = routers.DefaultRouter()
router.register(r'teams', TeamViewSet)
router.register(r'organisations', organisations_api_views.OrganisationViewSet)


urlpatterns = [
    path('team/users/', TeamUserView.as_view()),
    path('task/approve/', TaskApprovalView.as_view()),
    path('task/complete/', TaskCompleteView.as_view()),
    path('task/note/', TaskNoteView.as_view()),
    path('task/attachment/', TaskAttachmentView.as_view()),
    path('task/choices/', TaskChoiceView.as_view()),
    path('reorder/section/', SectionOrderView.as_view()),
    path('reorder/task/', TaskOrderView.as_view()),
    path('organisation/<int:org_id>/members/',OrganisationMembersView.as_view()),
    path('organisation/<int:organisation_id>/team/<int:team_id>/add/', AddTeamMemberView.as_view()),
    path('organisation/<int:organisation_id>/team/<int:team_id>/remove/', RemoveTeamMemberView.as_view()),
    path('make-admin', MakeAdminTeamMemberView.as_view()),
    path('remove-admin', RemoveAdminTeamMemberView.as_view()),
    path('contact-us/',ContactUsView.as_view()),
    path('', include(router.urls)),
]