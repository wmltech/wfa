from django import forms
from .models import Team


class TeamForm(forms.ModelForm):
    class Meta:
        model = Team
        exclude = ['created_by', 'modified_by', 'deleted', 'deleted_by', 'slug', 'is_active', 'is_system']
        # widgets = {'organisation': forms.HiddenInput()}
        widgets = {
            'name': forms.TextInput(attrs={'class': 'select form-control'}),
            'description': forms.Textarea(attrs={'class': 'select form-control'})
        }


class TeamInviteForm(forms.Form):
    email = forms.EmailField(required=True)


class DeleteTeamForm(forms.Form):
    confirm = forms.BooleanField(required=True, label='Are you sure you want to delete this team')

"""
class TeamForm(forms.Form):
    name = forms.CharField(max_length=60)
    organisation = models.
"""