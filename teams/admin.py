from django.contrib import admin
from .models import Team, TeamMember


# Register your models here.
@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug', 'organisation', 'created', 'created_by', 'modified', 'modified_by')


@admin.register(TeamMember)
class TeamMemberAdmin(admin.ModelAdmin):
    list_display = ('team', 'member', 'admin', 'active', 'joined', 'removed')
