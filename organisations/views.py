from django.http import Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.db.models import Q
from teams.models import Team, TeamMember
from projects.models import Project,Task
from uaa.models import User
from .models import Organisation
from .forms import OrganisationForm,OrganisationEditForm
from .decorators import user_is_organisation_member
from django.core.paginator import Paginator


# Create your views here.
@login_required
def dashboard(request):
    try:
        organisation = Organisation.objects.get(slug=request.session.get('U_ORGANISATION'))
    except Organisation.DoesNotExist:
        organisation = Organisation.objects.filter(team__teammember__member=request.user, active=True).distinct().first()
        if not organisation:
            return redirect('add_organisation')
    return redirect('my_tasks', slug=organisation.slug)



@login_required
@user_is_organisation_member
def organisation_home(request, slug=None):
    organisation = Organisation.objects.get(slug=slug)
    admin_team = Team.objects.get(organisation=organisation, name='Administrators')
    manager_team = Team.objects.get(organisation=organisation, name='Managers')
    admin_teammembers = TeamMember.objects.filter(team=admin_team)
    manager_teammembers = TeamMember.objects.filter(team=manager_team)
    request.session['U_ORGANISATION'] = organisation.slug
    form = OrganisationForm(request.POST or None, instance=organisation)
    if form.is_valid():
        print('Hello World')
        organisation = form.save(commit=False)
        organisation.modified_by = request.user
        organisation.save()
        return redirect('organisation_home',slug=organisation.slug)
    organisation_admin = TeamMember.objects.filter(team__organisation=organisation, member=request.user,
                                                   team=admin_team)
    organisation_manager = TeamMember.objects.filter(team__organisation=organisation, member=request.user,
                                                   team = manager_team)
    context = {'organisation': organisation,'admin_team':admin_team,'manager_team': manager_team,'admin_teammembers':admin_teammembers,'manager_teammembers':manager_teammembers,'organisation_admin':organisation_admin,'organisation_manager':organisation_manager,'form': form}
    return render(request, 'organisations/organisation_home.html', context=context)


@login_required
def add_organisation(request):
    form = OrganisationForm(request.POST or None)

    if form.is_valid():
        organisation = form.save(commit=False)
        organisation.created_by = request.user
        organisation.modified_by = request.user
        organisation.save()

        administrators_team = Team.objects.create(
            name='Administrators', organisation=organisation, created_by=request.user, modified_by=request.user,
            is_system=True
        )
        managers_team = Team.objects.create(
            name='Managers', organisation=organisation, created_by=request.user, modified_by=request.user,
            is_system=True
        )
        TeamMember.objects.create(team=administrators_team, member=request.user, admin=True, active=True)

        return redirect('organisation_home', slug=organisation.slug)

    return render(request, 'organisations/add_organisation.html', context={'form': form})


@login_required
@user_is_organisation_member
def organisation_members(request, slug, team_slug=None):
    organisation = Organisation.objects.get(slug=slug)
    query = request.GET.get('query', '')
    users = User.objects.filter(
        Q(teammember__team__organisation=organisation),
        Q(username__icontains=query) | Q(first_name__icontains=query) | Q(last_name__icontains=query)
    ).order_by('username').distinct()
    if team_slug:
        team = Team.objects.get(slug=team_slug, organisation=organisation)
        users = users.exclude(teammember__team=team)

    results = [{'id': user.id, 'username': user.username, 'email': user.email, 'text': user.get_full_name()} for user in users]
    return JsonResponse({'results': results})

@login_required
def search(request, slug):
    organisation = Organisation.objects.get(slug=slug)
    query = request.GET.get('query', '')
    teams = Team.objects.filter(name__icontains=query, organisation=organisation)
    projects = Project.objects.filter(name__icontains=query, organisation=organisation)
    tasks = Task.objects.filter(name__icontains=query, section__project__organisation=organisation)
    team_results = [{'id': team.id, 'text': team.name} for team in teams]
    project_results = [{'id': project.id, 'text': project.name} for project in projects]
    task_results = [{'id': task.id, 'text': task.name} for task in tasks]
    results = [
        {'text': 'Projects', 'children': project_results},
        {'text': 'Tasks', 'children': task_results},
        {'text': 'Teams', 'children': team_results},

    ]


    return JsonResponse({'results': results})


@login_required
@user_is_organisation_member
def organisation_teams(request, slug):
    organisation = Organisation.objects.get(slug=slug)
    query = request.GET.get('query', '')
    teams = Team.objects.filter(name__icontains=query, organisation=organisation)
    results = [{'id': team.id, 'text': team.name} for team in teams]
    return JsonResponse({'results': results})


@login_required
@user_is_organisation_member
def organisation_teams1(request, slug):
    organisation = get_object_or_404(Organisation, slug=slug)
    teams = Team.objects.filter(organisation__slug = slug,is_system=False).distinct().order_by('name').select_related('organisation')
    paginator = Paginator(teams, 15)
    page = request.GET.get('page', '')
    teams = paginator.get_page(page)
    context = {'teams': teams,'paginator': paginator,'organisation': organisation,'current_page': 'teams'}
    return render(request, 'organisations/organisation_teams.html', context=context)

@login_required
@user_is_organisation_member
def organisation_projects(request, slug):
    organisation = get_object_or_404(Organisation, slug=slug)
    projects = Project.objects.filter(organisation__slug=slug).distinct().order_by('-id').select_related('organisation','created_by')
    paginator = Paginator(projects, 15)
    page = request.GET.get('page', '')
    projects = paginator.get_page(page)
    context = {'projects': projects, 'paginator': paginator, 'organisation': organisation, 'current_page': 'projects'}
    return render(request, 'organisations/organisation_projects.html', context=context)

@login_required
@user_is_organisation_member
def edit_profile(request,slug):
    organisation = Organisation.objects.get(slug=slug)
    form = OrganisationEditForm(request.POST or None,instance=organisation)
    if form.is_valid():
        organisation = form.save(commit=False)
        organisation.modified_by = request.user
        organisation.modified = timezone.now()
        organisation.save()
        return redirect('site_home')
    return render(request, 'organisations/edit_organisation.html', {'form': form})



