from django.core.management.base import BaseCommand, CommandError
from projects.models import Project
from projects.helpers import update_project_status

class Command(BaseCommand):
    help = 'Update the project task status'

    def add_arguments(self, parser):
        parser.add_argument(
            '--id',
            action='store_true',
            dest='id',
            help='Update given project status id',
        )

    def handle(self, *args, **options):
        if options['id']:
            update_project_status(options['id'])
        else:
            for project in Project.objects.all():
                update_project_status(project.id)


