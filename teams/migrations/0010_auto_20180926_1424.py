# Generated by Django 2.1 on 2018-09-26 08:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('teams', '0009_team_is_system'),
    ]

    operations = [
        migrations.AlterField(
            model_name='teammember',
            name='admin',
            field=models.BooleanField(default=False),
        ),
    ]
