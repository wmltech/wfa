from django.urls import path
from . import views
from teams import views as teams_views
from projects import views as projects_views

#app_name = 'organisations'

urlpatterns = [
    path('', views.dashboard, name='organisation_dashboard'),
    path('add/', views.add_organisation, name='add_organisation'),
    path('<slug:slug>/edit-profile/', views.edit_profile, name='organisation_edit'),
    path('<slug:slug>/search/', views.search, name ='search'),
    path('<slug:slug>/', views.organisation_home, name='organisation_home'),

    path('<slug:slug>/teams/', views.organisation_teams, name='organisation_teams'),
    path('<slug:slug>/teams1/', views.organisation_teams1, name='organisation_teams1'),
    path('<slug:slug>/members/', views.organisation_members, name='organisation_members'),
    path('<slug:slug>/members/exclude/<slug:team_slug>/', views.organisation_members, name='organisation_members_exclude_team'),

    path('<slug:slug>/add/team/', teams_views.add_team, name='add_team'),
    path('<slug:slug>/add/project/', projects_views.add_project, name='add_project'),

    # Teams
    path('<slug:slug>/team/<slug:team_slug>/', teams_views.team_home, name='team_home'),
    path('<slug:slug>/team/<slug:team_slug>/change/', teams_views.change_team, name='change_team'),
    path('<slug:slug>/team/<slug:team_slug>/delete/', teams_views.delete_team, name='delete_team'),

    # Projects
    path('<slug:slug>/projects/', views.organisation_projects, name='organisation_projects'),
    path('<slug:slug>/project/<int:project_id>/', projects_views.project_home, name='project_home'),
    path('<slug:slug>/project/<int:project_id>/download/', projects_views.project_download, name='project_download'),
    path('<slug:slug>/projects/download/', projects_views.projects_download, name='projects_download'),
    path('<slug:slug>/project/<int:project_id>/task/<int:task_id>/', projects_views.task_home, name='task_home'),
    path('<slug:slug>/project/<int:project_id>/sections/', projects_views.get_project_sections, name='project_sections'),
    path('<slug:slug>/project/<int:project_id>/categories/', projects_views.get_project_categories, name='project_categories'),
    path('<slug:slug>/project/<int:project_id>/severities/', projects_views.get_project_severities,name='project_severities'),
    path('<slug:slug>/project/<int:project_id>/tasks/', projects_views.get_project_tasks,name='project_tasks'),

    path('<slug:slug>/project/<int:project_id>/section/<int:section_id>/change/', projects_views.change_section, name='change_section'),
    path('<slug:slug>/project/<int:project_id>/section/<int:section_id>/delete/', projects_views.delete_section, name='delete_section'),
    path('<slug:slug>/project/<int:project_id>/change/', projects_views.change_project, name='change_project'),
    path('<slug:slug>/project/<int:project_id>/delete/', projects_views.delete_project, name='delete_project'),

    #path('<slug:slug>/project/<int:project_id>/add/section/', projects_views.add_section, name='add_project_section'),
    # path('<slug:slug>/project/<int:project_id>/add/task/', projects_views.add_task, name='add_project_task'),
    path('<slug:slug>/project/<int:project_id>/add/task/<slug:task_type>/', projects_views.add_task, name='add_project_task'),
    path('<slug:slug>/project/<int:project_id>/add/task/<slug:task_type>/parent/<int:parent_id>/', projects_views.add_task, name='add_project_child_task'),

    path('<slug:slug>/project/<int:project_id>/task/<int:task_id>/change/',projects_views.edit_task,name='edit_task'),
    path('<slug:slug>/project/<int:project_id>/task/<int:task_id>/delete/',projects_views.delete_task,name='delete_task'),
    path('<slug:slug>/my-tasks/',projects_views.my_tasks,name='my_tasks'),
    path('<slug:slug>/my-team-tasks',projects_views.my_team_tasks,name='my_team_tasks'),
    path('<slug:slug>/project/<int:project_id>/task/<int:task_id>/clone/',projects_views.clone_task,name='clone_task'),

]
