from reportlab.pdfgen import canvas
from PyPDF2 import PdfFileWriter, PdfFileReader
import io
from reportlab.lib.pagesizes import letter
import comtypes.client
import win32com.client
import time


def pdfconverter(input, output):
    wdformat = 17
    print(input)
    in_file = input
    out_file = output

    # print out filenames
    print(in_file)
    print(out_file)

    # create COM object
    word = comtypes.client.CreateObject('Word.Application')
    # word = win32com.client.Dispatch('Word.Application')
    # key point 1: make word visible before open a new document
    word.Visible = True
    # key point 2: wait for the COM Server to prepare well.
    # time.sleep(3)

    # convert docx file 1 to pdf file 1
    doc = word.Documents.Open(in_file)

    # open docx file 1
    doc.SaveAs(out_file, FileFormat=wdformat)
    # conversion
    doc.Close()
    # close docx file 1
    word.Visible = False
    return out_file


def converter(inputfile, image):
    print(inputfile)
    c = canvas.Canvas('watermark.pdf')
    TO_RADIANS = (22 / 7) / 180
    # Draw the image at x, y. I positioned the x,y to be where i like here
    c.rotate(13)
    c.drawImage(image, 185, 770)

    # Add some custom text for good measure
    c.save()

    # Get the watermark file you just created
    watermark = PdfFileReader(open("watermark.pdf", "rb"))

    # Get our files ready
    output_file = PdfFileWriter()
    input_file = PdfFileReader(open(inputfile, "rb"))

    # Number of pages in input document
    page_count = input_file.getNumPages()

    # Go through all the input file pages to add a watermark to them
    for page_number in range(page_count):
        print("Watermarking page {} of {}".format(page_number, page_count))
        # merge the watermark with the page
        input_page = input_file.getPage(page_number)
        input_page.mergePage(watermark.getPage(0))
        # add page from input file to output document
        output_file.addPage(input_page)

    # finally, write "output" to document-output.pdf
    with open("FinalOutput.pdf", "wb") as outputStream:
        output_file.write(outputStream)

    pass





if __name__ == '__main__':
    pdffile = pdfconverter('D:/wfa-py/wfa/wfa/templates/one/toolchest/word.docx', 'D:/wfa-py/wfa/wfa/templates/one/toolchest/rr')
    converter('rr.pdf', 'logo2.jpg')

