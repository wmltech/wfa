from django.shortcuts import render, redirect
from .forms import ContactForm
from organisations.models import Organisation
from teams.models import Team
from django.http import JsonResponse
from projects.models import Task,Project

# Create your views here.
def site_home(request):
    if request.user.is_authenticated:
        return redirect('organisation_dashboard')
    return render(request, 'landing/Avilon/index.html')

def contact_us(request):
    form = ContactForm(request.POST or None)
    if form.is_valid():
        form.send_email()
        return redirect('contact_us_thanks')
    context = {'form': form}
    return render(request, 'toolchest/contact_us.html', context=context)


def contact_us_thanks(request):
    return render(request, 'toolchest/contact_us_thank_you.html')




