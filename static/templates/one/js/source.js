$(document).ready(function () {

    $('#on-canvas').hide();
    $(".loaderProject").hide();
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    var dependentval = $('.dep').children("select").val();
    console.log(dependentval);

    if (dependentval == "") {
        console.log("inside if")
        $('.duration').hide();
        $('.start_time').show();
        $('.end_time').show();
    }

    else {
        $('.duration').show();
        $('.start_time').hide();
        $('.end_time').hide();
    }

    $('.dep').change(function (event) {
        if (event.target.value == "") {
            $('.duration').hide();
            $('.start_time').show();
            $('.end_time').show();
        } else {
            $('.duration').show();
            $('.start_time').hide();
            $('.end_time').hide();
        }

    });

    function update_dependent_tasks() {
        var form = $('form#tj-add-task');
        var project = form.attr('data-project');
        var section = $("#id_dependent_on_section option:selected").val();
        var $el = $("#id_dependent_on_task");
        if (section) {
            var selectedTaskId =  $("#id_dependent_on_task option:selected").val();
            var url = '/api/task/choices/?section_id='+section+'&project_id='+project;
            $.ajax({
                url: url,
            }).done(function (results) {
                if (results.length === 0) {
                    $el.empty();
                    $el.attr('disbled', true);
                    $el.append($('<option value="">No tasks in this  section</option>'));
                } else {
                    $el.removeAttr('disabled');
                    $el.empty(); // remove old options
                    $el.append($('<option value="">--------- </option>'));
                    var selected = false;
                    $.each(results, function(key,value) {
                        if (selectedTaskId === value.id.toString()) {
                            $el.append($("<option></option>").attr({"value":  value.id, "selected": ''}).text(value.friendly_name));
                            selected = true;
                        } else {
                            $el.append($("<option></option>").attr("value", value.id).text(value.friendly_name));
                        }
                    });
                }
           }).fail(function (txt) {
           });
        } else {
            $el.attr('disabled', true);
        }
    }

    update_dependent_tasks();
    $('#id_dependent_on_section').change(function () {
        update_dependent_tasks();
    });


    function update_team_users() {
        var team = $("#id_assign_to_team option:selected").val();
        var $el = $("#id_assign_to_user");

        if (team) {
            var selectedUserId =  $("#id_assign_to_user option:selected").val();
            var url = '/api/team/users/?team_id='+team;
            $.ajax({
                url: url,
            }).done(function (results) {
                if (results.length === 0) {
                    $el.empty();
                    $el.attr('disbled', true);
                    $el.append($('<option value="">No users in this team</option>'));
                } else {
                    $el.removeAttr('disabled');
                    $el.empty(); // remove old options
                    $el.append($('<option value="">--------- </option>'));
                    var selected = false;
                    $.each(results, function(key,value) {
                        if (selectedUserId === value.id.toString()) {
                            $el.append($("<option></option>").attr({"value":  value.id, "selected": ''}).text(value.name));
                            selected = true;
                        } else {
                            $el.append($("<option></option>").attr("value", value.id).text(value.name));
                        }
                    });
                }
           }).fail(function (txt) {
           });
        } else {
            $el.attr('disabled', true);
        }
    }

    update_team_users();
    $('#id_assign_to_team').change(function () {
        console.log('Change Team');
        update_team_users();
    });





    var csrftoken = getCookie('csrftoken');
    console.log($('.add-member').attr('data-name'));
    var organisation = $('.add-member').attr('data-organisation');
    var team = $('.add-member').attr('data-team');


    $('.add-member').select2({
        tags: true,
        width: '80%',
        allowClear: true,
        placeholder: 'Search or enter email address to invite',
        tokenSeparators: [',', ' '],
        ajax: {
            url: '/organisation/' + organisation + '/members/exclude/' + team + '/',
            datatype: 'json',
            data: function (params) {
                var queryParameters = {query: params.term};
                return queryParameters;
            }
        }
    });

    $('#left-side-nav').css('min-height', $(window).height());

    $('#toggle-nav-left a').click(function (event) {

        event.preventDefault();
        $('#off-canvas').hide(500);
        var a = true;
        $('#on-canvas').removeClass('invisible').addClass('visible');
        $('#on-canvas').show(500);
        $('#toggle-nav-left').hide();
        var a = !a;
        // $('#off-canvas').removeClass('visible').addClass('invisible');
        // $('#on-canvas').removeClass('invisible').addClass('visible');

    });
    $('#toggle-nav-right a').click(function (event) {
        event.preventDefault();
        $('#on-canvas').hide(500);
        $('#on-canvas').removeClass('visible').addClass('invisible');
        $('#off-canvas').show(500);
        $('#toggle-nav-left').show();
        // $('#off-canvas').removeClass('invisible').addClass('visible');
    });

    $("#div_id_task_type").change(function () {
        var id = $(this).find("option:selected", this).attr("value");
        if (id == "P") {
            $("#div_id_parent").hide();
            $("#div_id_dependent_on").hide();
            $("#div_id_start_date").hide();
            $("#div_id_end_date").hide();
            $("#div_id_assigned_to_team").hide();
            $("#div_id_assigned_to_user").hide();
        } else {
            $("#div_id_parent").show();
            $("#div_id_dependent_on").show();
            $("#div_id_start_date").show();
            $("#div_id_end_date").show();
            $("#div_id_assigned_to_team").show();
            $("#div_id_assigned_to_user").show();
        }

    });

    $.datetimepicker.setLocale('en');

    $('#id_end_datetime').datetimepicker({
        format: "m/d/Y H:i:s",
        hourGrid: 4,
        minuteGrid: 10,
        showOn: "focus"
    });
    $('#id_start_datetime').datetimepicker({
        format: "m/d/Y H:i:s",
        hourGrid: 4,
        minuteGrid: 10,
        showOn: "focus"
    });
    $(".sortable").sortable({
        connectWith: 'ul.mainlist',
        beforeStop: function (ev, ui) {
            if ($(ui.item).hasClass('hasItems') && $(ui.placeholder).parent()[0] != this) {
                $(this).sortable('cancel');
            }
        },
        update: function (event, ui) {
            // console.log(ui.item.find('.col-6').children().val());
            if ($(ui.item).attr('data-type') == 'P') {
                console.log(ui.item.find('.task-title p')[0].firstChild);
                //console.log(ui.item.find('h6').children().html());
            }
            else {
                console.log(ui.item.find('h6').children().html());
            }
        }
    });
    $('ul.sublist').sortable({
        connectWith: 'ul.sublist',
        beforeStop: function (ev, ui) {
            if ($(ui.placeholder).parent()[0] != this) {
                $(this).sortable('cancel');
            }
        },
        update: function (event, ui) {
            console.log(ui.item.find('h6').children().html());
        }
    });
    $(".sortable").disableSelection();

    $('#addMember').on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        $('.loaderProject').show();
        var addForm = $('.modal-body form');
        console.log(addForm);
        var organisation = addForm.attr('data-organisation');
        var team = addForm.attr('data-team');
        var data = addForm.serializeArray();
        var url = '/api/organisation/' + organisation + '/team/' + team + '/add/';
        var parent = $(this).parents('.modal-footer');
        console.log(parent);
        if (parent.has($(".teamerror"))) {
            parent.find('.teamerror').empty();
        }
        arr = [];
        if (data.length > 1) {
            arr.push(data[1].value);
        }
        var formData = new FormData();
        formData.append('members', arr);
        formData.append('test', 'Hello');
        console.log(formData);
        console.log('Before AJAX', new Date())
        $.ajax({
            url: url,
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            headers: {
                // 'Content-Type': 'multipart/form-data',
                "X-CSRFToken": csrftoken
            },
        }).done(function (results) {
            $('#add-team-member').modal('hide');
            $('.loaderProject').hide();
           console.log('After AJAX', new Date())
           console.log('Done');
           console.log(results);
           var email = results.email;
           var listOfMembers=$('table');
           $(".teamerror").hide();
           $(".text-danger").hide();

           // window.location.reload();
           $("<tr class='teamMemberList'><td><input type='checkbox' class='checkTeam' value="+results.id+"></td><td>"+results.full_name+"</td><td>"+email+"</td><td class='admincheck'></td></tr>").appendTo(listOfMembers);
           if(checklist.length == 0) {
                $('.remove-team-member').toggleClass('disabled');
                $('.make-admin-team-member').toggleClass('disabled');
                $('.remove-admin-team-member').toggleClass('disabled');
           }
           // $('.modal-body').find(".add-member").val(null).trigger('change');
       }).fail(function (txt) {
           console.log('Failed');
           console.log(txt);
           $('.loaderProject').hide();
           parent.prepend("<div class='row justify-content-md-center teamerror'>"+txt.responseJSON.members[0]+"</div>");
       });
    });

    //$('.remove-team-member').on('click', function() {
    $('.remove-team-member').on('click', function (event) {

        event.preventDefault();
        event.stopPropagation();
        if (checklist.length == 0) {
            for (i = 0; i < $('.checkTeam').length; i++) {
                if ($('.checkTeam')[i].checked == true) {
                    checklist.push($('.checkTeam')[i].value)
                }
            }
        }
        $('.loaderProject').show();
        var addForm = $(this);
        var organisation = addForm.attr('data-organisation');
        var team = addForm.attr('data-team');
        console.log("checklist", checklist);
        var formData = new FormData();
        for (i = 0; i < checklist.length; i++) {
            formData.append('members', checklist[i]);
        }
        formData.append('test', 'Hello');
        // console.log('After formdata append');
        console.log(formData)
        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }

        var url = '/api/organisation/' + organisation + '/team/' + team + '/remove/';
        $.ajax({
            url: url,
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            headers: {
                // 'Content-Type': 'multipart/form-data',
                "X-CSRFToken": csrftoken
            },
        }).done(function (results) {
            $('.loaderProject').hide();
            console.log('Done');

            for (i = 0; i < checklist.length; i++) {
                for (j = 0; j < $('.checkTeam').length; j++) {
                    if ($('.checkTeam')[j].value == checklist[i]) {
                        $('.checkTeam')[j].closest('tr').remove();
                    }
               }
            }
            // var test=$('.manage-teams li a[data-member="' + member + '"]');

            /* if($(test)) {
             $('.manage-teams li a[data-member="' + member + '"]').parent().remove();
             }*/

        }).fail(function (txt) {
            console.log('Failed');
            console.log(txt);
        });
    });

    $('.manage-teams').on('click', '.make-admin-team-member', function (event) {

        event.preventDefault();
        event.stopPropagation();
        if (checklist.length == 0) {
            for (i = 0; i < $('.checkTeam').length; i++) {
                if ($('.checkTeam')[i].checked == true) {
                    checklist.push($('.checkTeam')[i].value)
                }
            }
        }
        $('.loaderProject').show();
        var addForm = $(this);
        var member = addForm.attr('data-member');
        var organisation = addForm.attr('data-organisation');
        var team = addForm.attr('data-team');
        var formData = new FormData();
        for (i = 0; i < checklist.length; i++) {
            formData.append('member', checklist[i]);
        }
        formData.append('organisation', organisation);
        formData.append('team', team);
        formData.append('test', 'Hello');
        // console.log('After formdata append');
        console.log(formData)
        var obj = {"organisation": organisation, "team": team, "member": member, "csrfmiddlewaretoken": csrftoken};
        console.log(obj);
        // var data = JSON.stringify(obj);
        var url = '/api/make-admin';
        $.ajax({
            url: url,
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            headers: {
                // 'Content-Type': 'multipart/form-data',
                "X-CSRFToken": csrftoken
            },
        }).done(function (results) {
            $('.loaderProject').hide();
            console.log('Done');
            for (i = 0; i < checklist.length; i++) {
                for (j = 0; j < $('.checkTeam').length; j++) {
                    if ($('.checkTeam')[j].value == checklist[i]) {
                        var cols = $('.teamMemberList')[j].lastElementChild;
                        console.log(cols);
                        while (cols.children.length == 0) {
                            $("<span class='table_admin'><i class='fa fa-check'></i></span>").appendTo(cols);
                            break;
                        }
                    }
                }
            }


        }).fail(function (txt) {
            console.log('Failed');
            console.log(txt);
        });
    });

    $('.remove-admin-team-member').on('click', function (event) {

        event.preventDefault();
        event.stopPropagation();
        if (checklist.length == 0) {
            for (i = 0; i < $('.checkTeam').length; i++) {
                if ($('.checkTeam')[i].checked == true) {
                    checklist.push($('.checkTeam')[i].value)
                }
            }
        }
        $('.loaderProject').show();
        var addForm = $(this);
        var member = addForm.attr('data-member');
        var organisation = addForm.attr('data-organisation');
        var team = addForm.attr('data-team');
        var formData = new FormData();
        for (i = 0; i < checklist.length; i++) {
            formData.append('member', checklist[i]);
        }
        formData.append('organisation', organisation);
        formData.append('team', team);
        formData.append('test', 'Hello');
        // console.log('After formdata append');
        console.log(formData)
        var obj = {"organisation": organisation, "team": team, "member": member, "csrfmiddlewaretoken": csrftoken};
        console.log(obj);
        // var data = JSON.stringify(obj);
        var url = '/api/remove-admin';
        $.ajax({
            url: url,
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            headers: {
                // 'Content-Type': 'multipart/form-data',
                "X-CSRFToken": csrftoken
            },
        }).done(function (results) {
            $('.loaderProject').hide();
            console.log('Done');
            for (i = 0; i < checklist.length; i++) {
                for (j = 0; j < $('.checkTeam').length; j++) {
                    if ($('.checkTeam')[j].value == checklist[i]) {
                        var cols = $('.teamMemberList')[j].lastElementChild;
                        while (cols.children.length != 0) {
                            cols.children[0].remove();
                        }
                    }
                }
            }
        }).fail(function (txt) {
            console.log('Failed');
            console.log(txt);
        });
    });


    $('.post-comments-list').on('submit', '.comment-form', function (event) {
        event.preventDefault();
        event.stopPropagation();
        var commentForm = $(this);
        var post = commentForm.attr('data-post-id');
        var parent = commentForm.attr('data-parent-id');
        var data = commentForm.serializeArray();
        data.push({name: "post", value: post});
        data.push({name: "parent", value: parent});
        data.push({name: "csrfmiddlewaretoken", value: csrftoken});
        commentForm.find('.form-error').remove();
        $.ajax({
            url: '/api/v1/post/comment/',
            type: 'POST',
            data: data
        }).done(function (results) {
            var parentComment = commentForm.closest('.post-comment');
            parentComment.find('a.comment-reply').removeClass('hide')
            var children = parentComment.find('ul.post-comments-list');
            commentForm.remove();
            if (children.length) {
                children.prepend(results['comment_html'])
            } else {
                parentComment.append('<ul class="post-comments-list no-bullet">' + results['comment_html'] + '</ul>')
            }
        }).fail(function (txt) {
            //console.log(txt.responseText);
            var msg = '';
            $.each($.parseJSON(txt.responseText), function (key, value) {
                msg += value + '</br>'
            });
            commentForm.find('textarea').addClass('is-invalid-input');
            commentForm.find('textarea').after('<span class="form-error is-visible">' + msg + '</span>');
        });

    });

    var organisation_slug = null;


    $('#id_section').select2({
        tags: true,
        placeholder: 'Select section',
        allowClear: true,
        ajax: {
            url: function () {
                var form = $(this).closest('form');
                var organisation = form.attr('data-organisation');
                var project = form.attr('data-project');
                return '/organisation/' + organisation + '/project/' + project + '/sections/'
            },
            datatype: 'json',
            data: function (params) {
                var queryParameters = {query: params.term};
                return queryParameters;
            }
        }
    });

    $('#id_category').select2({
        tags: true,
        placeholder: 'Select category',
        allowClear: true,
        ajax: {
            url: function () {
                var form = $(this).closest('form');
                var organisation = form.attr('data-organisation');
                var project = form.attr('data-project');
                return '/organisation/' + organisation + '/project/' + project + '/categories/'
            },
            datatype: 'json',
            data: function (params) {
                var queryParameters = {query: params.term};
                return queryParameters;
            }
        }
    });

    $('#id_severity').select2({
        tags: true,
        placeholder: 'Select severity',
        allowClear: true,
        ajax: {
            url: function () {
                var form = $(this).closest('form');
                var organisation = form.attr('data-organisation');
                var project = form.attr('data-project');
                return '/organisation/' + organisation + '/project/' + project + '/severities/'
            },
            datatype: 'json',
            data: function (params) {
                var queryParameters = {query: params.term};
                return queryParameters;
            }
        }
    });

    $('#id_dependent_on').select2({
        tags: true,
        placeholder: 'If this task depends on other task, please select',

        ajax: {
            url: function () {
                console.log('Hello');
                var form = $(this).closest('form');
                var organisation = form.attr('data-organisation');
                var project = form.attr('data-project');
                return '/organisation/' + organisation + '/project/' + project + '/tasks/'
            },
            datatype: 'json',
            data: function (params) {
                var queryParameters = {query: params.term};
                return queryParameters;
            }
        }
    });

    $('.assign_to_team1').select2({
        placeholder: 'Search or enter email address to invite',
        ajax: {
            url: function () {
                var form = $(this).closest('form');
                var organisation = form.attr('data-organisation');
                return '/organisation/' + organisation + '/teams/';
            },
            datatype: 'json',
            data: function (params) {
                var queryParameters = {
                    query: params.term,
                };
                return queryParameters;
            }
        }
    });

    $('.assign_to_user1').select2({
        placeholder: 'Search or enter email address to invite',
        ajax: {
            url: function () {
                var form = $(this).closest('form');
                var organisation = form.attr('data-organisation');
                return '/organisation/' + organisation + '/members/';
            },
            datatype: 'json',
            data: function (params) {
                var queryParameters = {
                    query: params.term,
                };
                return queryParameters;
            }
        }
    });


    $('#approvalModal').on('show.bs.modal', function (event) {
        var link = $(event.relatedTarget);
        var task = link.data('task');
        var action = link.data('action');
        var form = $('form#approvalTaskForm');
        form.data('task', task);
        form.data('action', action);
        var button = form.find('button.action');
        if (action === 'approve') {
            button.html('Approve');
            button.addClass('btn-success').removeClass('btn-danger');
        } else if (action === 'reject') {
            button.html('Reject');
            button.addClass('btn-danger').removeClass('btn-success');
        }
    });

    $('#approvalModal').on('submit', '#approvalTaskForm', function (event) {
        event.preventDefault();
        $(".loaderProject").show();
        var form = $(this);
        var task = form.data('task');
        var action = form.data('action');
        var data = form.serializeArray();
        form.find('.form-error').remove();
        data.push(
            {name: "task", value: form.data('task')},
            {name: "action", value: form.data('action')},
            {name: "csrfmiddlewaretoken", value: csrftoken}
        );
        $.ajax({
            url: '/api/task/approve/',
            type: 'POST',
            data: data,
        }).done(function (results) {
            $(".loaderProject").hide();
            $('#approvalModal').modal('hide');
            location.reload()
        }).fail(function (txt) {
            var msg = '';
            $.each($.parseJSON(txt.responseText), function (key, value) {
                msg += value + '</br>'
            });
            form.find('textarea').addClass('is-invalid-input');
            form.find('textarea').after('<span class="form-error is-visible">' + msg + '</span>');
        });

    });


    $('#completeModal').on('show.bs.modal', function (event) {
        var link = $(event.relatedTarget);
        var task = link.data('task');
        var form = $('form#approvalTaskForm');
        form.data('task', task);
    });

    $('#completeModal').on('submit', '#approvalTaskForm', function (event) {
        event.preventDefault();
        $(".loaderProject").show();
        var form = $(this);
        var task = form.data('task');
        var data = form.serializeArray();
        form.find('.form-error').remove();
        data.push(
            {name: "task", value: form.data('task')},
            {name: "csrfmiddlewaretoken", value: csrftoken}
        );
        $.ajax({
            url: '/api/task/complete/',
            type: 'POST',
            data: data,
        }).done(function (results) {
            $(".loaderProject").show();
            $('#approvalModal').modal('hide');
            location.reload()
        }).fail(function (txt) {
            var msg = '';
            $.each($.parseJSON(txt.responseText), function (key, value) {
                msg += value + '</br>'
            });
            form.find('textarea').addClass('is-invalid-input');
            form.find('textarea').after('<span class="form-error is-visible">' + msg + '</span>');
        });
    });


    $('#noteModal').on('show.bs.modal', function (event) {
        var link = $(event.relatedTarget);
        var task = link.data('task');
        var form = $('form#addNoteForm');
        form.data('task', task);
    });

    $('#noteModal').on('submit', '#addNoteForm', function (event) {
        event.preventDefault();
        $(".loaderProject").show();
        var form = $(this);
        var task = form.data('task');
        var data = form.serializeArray();
        form.find('.form-error').remove();
        data.push(
            {name: "task", value: form.data('task')},
            {name: "csrfmiddlewaretoken", value: csrftoken}
        );
        console.log(data);

        $.ajax({
            url: '/api/task/note/',
            type: 'POST',
            data: data
        }).done(function (results) {
            $(".loaderProject").hide();
            $('#noteModal').modal('hide');
            location.reload()
        }).fail(function (txt) {
            var msg = '';
            $.each($.parseJSON(txt.responseText), function (key, value) {
                msg += value + '</br>'
            });
            form.find('textarea').addClass('is-invalid-input');
            form.find('textarea').after('<span class="form-error is-visible">' + msg + '</span>');
        });

    });

    $('#attachmentModal').on('show.bs.modal', function (event) {
        var link = $(event.relatedTarget);
        var task = link.data('task');
        var form = $('form#addAttachmentForm');
        form.data('task', task);
    });


    $('#attachmentModal').on('submit', '#addAttachmentForm', function (event) {
        event.preventDefault();
        $(".loaderProject").show();
        var form = $(this);

        var formData = new FormData();
        var dataArray = form.serializeArray();

        for (i = 0; i < dataArray.length; i++) {
            formData.append(dataArray[i].name, dataArray[i].value);
        }

        formData.append('task', form.data('task'));
        formData.append('file', $('#id_file').prop('files')[0]);

        console.log('Hello World');
        for (var pair of formData.entries()) {
            console.log(pair[0] + ', ' + pair[1]);
        }

        form.find('.form-error').remove();

        $.ajax({
            url: '/api/task/attachment/',
            type: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            headers: {
                // 'Content-Type': 'multipart/form-data',
                "X-CSRFToken": csrftoken
            },
        }).done(function (results) {
            $(".loaderProject").hide();
            $('#attachmentModal').modal('hide');
            location.reload()
        }).fail(function (txt) {
            var msg = '';
            $.each($.parseJSON(txt.responseText), function (key, value) {
                msg += value + '</br>'
            });
            form.find('textarea').addClass('is-invalid-input');
            form.find('textarea').after('<span class="form-error is-visible">' + msg + '</span>');
        });
    });

    $(".more").toggle(function () {
        $(this).text("less..").siblings(".complete").show();
    }, function () {
        $(this).text("more..").siblings(".complete").hide();
    });
    checklist = []
    $("#checkal").click(function () {
        if ($(this).prop("checked") == true) {
            $(".checkTeam").prop('checked', $(this).prop('checked'));
            $('.remove-team-member').removeClass("disabled");
            $('.make-admin-team-member').removeClass("disabled");
            $('.remove-admin-team-member').removeClass("disabled");
        } else {
            $(".checkTeam").prop('checked', $(this).prop('checked'));
            checklist = [];
            $('.remove-team-member').toggleClass("disabled");
            $('.make-admin-team-member').toggleClass("disabled");
            $('.remove-admin-team-member').toggleClass("disabled");
        }
    });
    $(".checkOrganisation").click(function () {
        if ($(this).prop("checked") == true) {
            $(".checkTeam").prop('checked', $(this).prop('checked'));
            $(".checkManager").prop('checked', $(this).prop('checked'));
            $('.remove-team-member').removeClass("disabled");
        } else {
            $(".checkTeam").prop('checked', $(this).prop('checked'));
            $(".checkManager").prop('checked', $(this).prop('checked'));
            checklist = [];
            $('.remove-team-member').toggleClass("disabled");
        }
    });

    function checkTeams (thi) {
        console.log('hi',thi);
        this_ = thi;
        ischeckedTeam = false;
        ischeckedManager = false;
        temp=0
        if(this_.prop("checked") == true){
            $('.remove-team-member').removeClass("disabled");
            $('.make-admin-team-member').removeClass("disabled");
            $('.remove-admin-team-member').removeClass("disabled");
            if(checklist.length > 0) {
                for(i =0 ; i< checklist.length; i++) {
                    if(checklist[i] == this_[0].value) {
                        temp = temp + 1;
                    }
                    if(temp ==0) {
                        checklist.push(this_[0].value)
                    }
                }
            } else {
                checklist.push(this_[0].value)
            }
        } else if (this_.prop("checked") == false) {
            for (i = 0; i < checklist.length; i++) {
                if (checklist[i] == this_[0].value) {
                    checklist.splice(i, 1)
                }
            }
            if (checklist.length == 0) {
                for (i = 0; i < $('.checkTeam').length; i++) {
                    if ($('.checkTeam')[i].checked == true) {
                        ischeckedTeam = true;
                    }
                }
                for (i = 0; i < $('.checkManager').length; i++) {
                    if ($('.checkManager')[i].checked == true) {
                        ischeckedManager = true;
                    }
                }
                if (!ischeckedTeam) {
                    $('.remove-team-member').toggleClass('disabled');
                    $('.make-admin-team-member').toggleClass('disabled');
                    $('.remove-admin-team-member').toggleClass('disabled');
                }
                if(!ischeckedManager) {
                    $('.remove-team-member').toggleClass('disabled');
                    $('.make-admin-team-member').toggleClass('disabled');
                    $('.remove-admin-team-member').toggleClass('disabled');
                }

                $("#checkal").prop('checked', false);
                $(".checkOrganisation").prop('checked', false);
            }
        }
    };

    $('.checkManager').on('click', function () {
        console.log($(this));
        checkTeams($(this));
    });
    $('.checkTeam').on('click', function () {
        checkTeams($(this));
    } ) ;
    checkedlist = []
    $("#checkall").click(function () {
        if ($(this).prop("checked") == true) {
            $(".check").prop('checked', $(this).prop('checked'));
            $('#download .btn-link').removeClass("disabled");
        } else {
            $(".check").prop('checked', $(this).prop('checked'));
            checkedlist = [];
            $('#download .btn-link').toggleClass('disabled');
        }
    });

    $('.check').on('click', function () {
        ischecked = false;
        temp=0;
        if($(this).prop("checked") == true){
            $('#download .btn-link').removeClass("disabled");
            if(checkedlist.length > 0) {
                for(i =0 ; i< checkedlist.length; i++) {
                    if (checkedlist[i] == $(this)[0].value) {
                        temp = temp+1;
                    }
                }
                if(temp == 0) {
                    checkedlist.push($(this)[0].value);
                }
            } else {
                checkedlist.push($(this)[0].value)
            }
        } else if ($(this).prop("checked") == false) {
            for (i = 0; i < checkedlist.length; i++) {
                if (checkedlist[i] == $(this)[0].value) {
                    checkedlist.splice(i, 1)
                }
            }
            if (checkedlist.length == 0) {
                for (i = 0; i < $('.check').length; i++) {
                    if ($('.check')[i].checked == true) {
                        ischecked = true;
                    }
                }
                if (!ischecked) {
                    $('#download .btn-link').toggleClass('disabled');
                }

                $("#checkall").prop('checked', $(this).prop("checked"));
            }
        }
    });
    function remove(arr, item) {
        for (var i = arr.length; i--;) {
            if (arr[i] === item) {
                arr.splice(i, 1);
            }
            return arr;
        }
    }

    $('#download').click(function () {
        isChecked = false
        for (i = 0; i < $('.check').length; i++) {
            while ($('.check')[i].checked == true) {
                isChecked = true;
                break;
            }
        }
        if (isChecked) {
            console.log($(this).children());
            var downloadProject = $(this);
            console.log(downloadProject);
            var organisation = downloadProject.attr('data-organisation');
            if (checkedlist.length == 0) {
                for (i = 0; i < $('.check').length; i++) {
                    if ($('.check')[i].checked == true) {
                        checkedlist.push($('.check')[i].value)
                    }
                }
            } else {
                console.log(checkedlist.toString())
            }
            lis = checkedlist.toString()
            $('#download a')[0].href = $('#download a')[0].href.split('=')[0] + "=" + lis;
        } else {
            if ($('#download .disabled')) {
                console.log("disable present")
            } else if ($('#download .btn-link')) {
                $('#download .btn-link').toggleClass('disabled');
            }
        }
    });



});








