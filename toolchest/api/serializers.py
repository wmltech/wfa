from rest_framework import serializers
from django.core.mail import send_mail
from django.conf import settings
from django.http import HttpResponse
from django.template import Context
from django.template.loader import render_to_string, get_template
from django.core.mail import EmailMessage


class ContactUsSerializer(serializers.Serializer):

    name = serializers.CharField()
    subject = serializers.CharField()
    email = serializers.EmailField()
    message = serializers.CharField()

    def send_email(self, **kwargs):
        emails = []
        admin_names= []
        for name, email in settings.ADMINS:
            emails.append(email)
            admin_names.append(name)


        message = self.data.get('message')
        from_email = self.data.get('email')
        subject = self.data.get('subject')
        name = self.data.get('name').title()
        """send_mail(
            subject,
            message,
            from_email,
            emails,
            fail_silently=False,
        )"""
        ctx = {
            'admin_names': admin_names,
            'message': message,
            'name': name
        }

        message = get_template('toolchest/api/email.html').render(ctx)
        msg = EmailMessage(subject, message, to=emails, from_email=from_email)
        msg.content_subtype = 'html'
        msg.send()
