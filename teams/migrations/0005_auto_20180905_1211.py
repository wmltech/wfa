# Generated by Django 2.1 on 2018-09-05 06:41

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('teams', '0004_auto_20180905_1143'),
    ]

    operations = [
        migrations.AlterField(
            model_name='team',
            name='deleted',
            field=models.DateTimeField(null=True),
        ),
        migrations.AlterField(
            model_name='team',
            name='deleted_by',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='team_deleted_by', to=settings.AUTH_USER_MODEL),
        ),
    ]
