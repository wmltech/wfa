from django.core import signing
from django.utils import timezone
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import get_user_model
from uaa.forms import RegistrationForm, CompleteRegistrationForm,EditProfileForm
from .helpers import get_base_url, send_user_creation_email


# Create your views here.
def register(request):
    if request.user.is_authenticated:
        return redirect('site_home')
    form = RegistrationForm(request.POST or None)

    if form.is_valid():
        new_user = form.save()
        new_user.is_active = False
        new_user.save()
        send_user_creation_email(new_user, get_base_url(request))
        return redirect('uaa_register_complete')
    return render(request, 'uaa/register.html', {'form': form})


def register_complete(request):
    if request.user.is_authenticated:
        return redirect('site_home')
    return render(request, 'uaa/register_complete.html')


def activate(request, activation_key):
    username = signing.loads(activation_key, 'uaa')
    user = get_object_or_404(get_user_model(), username=username)
    form = CompleteRegistrationForm(
        request.POST or None,
        initial={'first_name': user.first_name, 'last_name': user.last_name}
    )
    if form.is_valid():
        first_name = form.cleaned_data['first_name']
        last_name = form.cleaned_data['last_name']
        password = form.cleaned_data['password']
        user.first_name = first_name
        user.last_name = last_name
        user.is_active = True
        user.set_password(password)
        user.save()
        return redirect('uaa_activate_complete')
    return render(request, 'uaa/activate.html', {'form': form})


def activate_complete(request):
    if request.user.is_authenticated:
        return redirect('site_home')
    return render(request, 'uaa/activate_complete.html')


def edit_profile(request):
    user = get_object_or_404(get_user_model(), username=request.user.username)
    print(user)
    form = EditProfileForm(request.POST or None,instance=user)
    if form.is_valid():
        user = form.save(commit=False)
        user.modified_by = request.user
        user.modified = timezone.now()
        user.save()
        return redirect('site_home')
    return render(request, 'uaa/edit_profile.html', {'form': form})


