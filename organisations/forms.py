from django import forms
from .models import Organisation


class OrganisationForm(forms.ModelForm):
    class Meta:
        model = Organisation
        exclude = ['created_by', 'modified_by', 'slug', 'active','deleted','deleted_by']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'select form-control'}),
            'project_prefix': forms.TextInput(attrs={'class': 'select form-control'}),
            'task_prefix': forms.TextInput(attrs={'class': 'select form-control'})
        }

class OrganisationEditForm(forms.ModelForm):
    class Meta:
        model = Organisation
        fields = ['name']

