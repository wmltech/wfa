from django.shortcuts import render, Http404, redirect, get_object_or_404
from django.utils import timezone
from django import forms
from django.contrib.auth.decorators import login_required
from organisations.models import Organisation
from teams.models import Team,TeamMember
from projects.models import Project
from uaa.models import User
from .forms import TeamForm,TeamInviteForm, DeleteTeamForm
from organisations.decorators import user_is_organisation_member
from uaa.models import User


# Create your views here.
@user_is_organisation_member
def team_home(request, slug, team_slug):
    teams = Team.objects.filter(organisation__slug=slug, organisation__team__teammember__member=request.user).distinct()
    team = get_object_or_404(teams, slug=team_slug)
    teammembers = TeamMember.objects.filter(team=team)
    teamadmin = TeamMember.objects.filter(team=team, member=request.user, admin=True) | TeamMember.objects.filter(team__organisation=team.organisation, member=request.user, team__name__in =('Administrators','Managers'))

    print(teamadmin)
    form = TeamForm(request.POST or None ,instance=team)
    form.fields['organisation'] = forms.ModelChoiceField(Organisation.objects.filter(id=team.organisation.id), initial=team.organisation, widget=forms.HiddenInput())
    print(team.organisation)
    print(form)
    if form.is_valid():
        print('Hello World')
        team = form.save(commit=False)
        team.modified_by = request.user
        team.save()
        return redirect('team_home', slug=team.organisation.slug, team_slug=team.slug)
    context = {'teammembers': teammembers, 'form': form, 'team': team, 'current_page': 'teams', 'teamadmin':teamadmin}
    return render(request, 'teams/team_home.html', context=context )


def add_team(request, slug):
    organisations = Organisation.objects.filter(
        slug=slug, team__teammember__member=request.user, active=True
    ).distinct()
    if len(organisations) != 1:
        raise Http404
    organisation = organisations.first()
    teams = Team.objects.filter(organisation=organisation)
    projects = Project.objects.filter(organisation=organisation)
    print(organisation)

    form = TeamForm(request.POST or None)
    form.fields['organisation'] = forms.ModelChoiceField(Organisation.objects.filter(id=organisation.id))
    form.fields['organisation'].widget = forms.HiddenInput()
    form.fields['organisation'].initial = organisation
    if form.is_valid():
        team = form.save(commit=False)
        team.created_by = request.user
        team.modified_by = request.user
        team.save()
        #TeamMember.objects.create(team=team, member=request.user, admin=True, active=True)
        return redirect('team_home', slug=organisation.slug, team_slug=team.slug)

    context = {'organisation': organisation, 'teams': teams, 'projects': projects, 'form': form}
    return render(request, 'teams/add_team.html', context=context)

@login_required
def change_team(request, slug, team_slug):
    teams = Team.objects.filter(organisation__slug=slug, organisation__team__teammember__member=request.user).distinct()
    team = get_object_or_404(teams, slug=team_slug)
    form = TeamForm(request.POST or None, instance=team)
    # form.fields['organisation'] = forms.ModelChoiceField(Organisation.objects.filter(id=team.organisation.id))
    form.fields['organisation'].widget = forms.HiddenInput()
    # form.fields['organisation'].initial = team.organisation
    if form.is_valid():
        team = form.save(commit=False)
        team.modified_by = request.user
        team.save()
    context = {'form': form}
    return render(request, 'teams/change_team.html', context=context)


@login_required
#@user_is_organisation_member
def delete_team(request, slug, team_slug):
    form = DeleteTeamForm(request.POST or None)
    teams = Team.objects.filter(organisation__slug=slug, organisation__team__teammember__member=request.user).distinct()
    team = get_object_or_404(teams, slug=team_slug)
    if form.is_valid():
        team.is_active = False
        team.deleted = timezone.now()
        team.deleted_by = request.user
        team.save()
        return render(request, 'teams/delete_team_done.html')
    context = {'form': form, 'team': team}
    return render(request, 'teams/delete_team.html', context=context)



def add_member_to_team(request, slug, team_slug):
    if request.method == 'POST':
        user = request.POST.get('user')

        try:
            user = User.objects.get(pk=user)
        except User.DoesNotExist:
            from django.core.validators import validate_email
            try:
                validate_email(user)
            except:
                print('Enter valid email address')

