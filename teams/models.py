from django.conf import settings
from django.db import models
from organisations.models import Organisation
from toolchest.slugger import unique_slugify


# Create your models here.
class Team(models.Model):
    name = models.CharField(max_length=60)
    slug = models.SlugField(max_length=60, blank=True)
    description = models.TextField(max_length=2000, blank=True)
    organisation = models.ForeignKey(Organisation, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)
    is_system = models.BooleanField(default=False)

    # Additional Info
    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='team_created_by', on_delete=models.CASCADE)
    modified = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='team_modified_by', on_delete=models.CASCADE)
    deleted = models.DateTimeField(blank=True, null=True)
    deleted_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='team_deleted_by', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return '{} -> {}'.format(self.organisation, self.name)

    class Meta:
        unique_together = ('name', 'organisation')

    def save(self, **kwargs):
        """Override save method to implement unique slug"""
        if not self.id:
            qs = self.__class__.objects.filter(organisation=self.organisation)
        else:
            qs = self.__class__.objects.filter(organisation=self.organisation).exclude(pk=self.id)
        if not self.slug:
            unique_slugify(self, self.name, 'slug', qs)
        else:
            unique_slugify(self, self.slug, 'slug', qs)
        super(Team, self).save()


class TeamMember(models.Model):
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    member = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    admin = models.BooleanField(default=False)
    active = models.BooleanField(default=True)
    # TODO Add joined by and removed by field so we can tell who joined/removed
    joined = models.DateTimeField(auto_now_add=True)
    removed = models.DateTimeField(blank=True, null=True)


    def __str__(self):
        return '{} -> {}'.format(self.team, self.member)

    class Meta:
        unique_together = ('team', 'member')
