from rest_framework import status
from rest_framework import generics
from rest_framework.response import Response

from toolchest.api.serializers import ContactUsSerializer

class ContactUsView(generics.GenericAPIView):
    serializer_class = ContactUsSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.send_email()
        return Response({}, status=status.HTTP_201_CREATED)