from django import forms
from django.conf import settings
from django.template.loader import  get_template
from django.core.mail import EmailMessage

class ContactForm(forms.Form):

    name = forms.CharField()
    subject = forms.CharField()
    email = forms.EmailField()
    message = forms.CharField(widget=forms.Textarea)


    def send_email(self):
        emails = []
        admin_names = []
        for name, email in settings.ADMINS:
            emails.append(email)
            admin_names.append(name)

        message = self.data.get('message')
        from_email = self.data.get('email')
        subject = self.data.get('subject')
        name = self.data.get('name').title()
        ctx = {
            'admin_names': admin_names,
            'message': message,
            'name': name
        }

        message = get_template('toolchest/api/email.html').render(ctx)
        msg = EmailMessage(subject, message, to=emails, from_email=from_email)
        msg.content_subtype = 'html'
        msg.send()