var gulp        = require('gulp');
var $           = require('gulp-load-plugins')();
var concat      = require('gulp-concat');



var config = {
    sassPaths: [
        'node_modules/bootstrap/scss',
        'node_modules/@fortawesome/fontawesome-free/scss',
        'node_modules/select2/dist/css'
    ],
    jsPaths: [
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/bootstrap/dist/js/bootstrap.bundle.min.js',
        'node_modules/select2/dist/js/select2.min.js',
        '../static/js/jquery.datetimepicker.full.min.js',
        '../static/js/source.js',
        '../static/js/jquery-ui.min.js'
    ],
    fontPaths: [
        'node_modules/@fortawesome/fontawesome-free/webfonts/*'
    ]
};

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src(['scss/*.scss'])
        .pipe($.sass({includePaths: config.sassPaths}))
        .pipe(gulp.dest("../static/css"));
});

gulp.task('javascript', function() {
    return gulp.src(config.jsPaths)
        .pipe(concat('app.js'))
        .pipe(gulp.dest('../static/js'));
});


gulp.task('fonts', function() {
    return gulp.src(config.fontPaths)
        .pipe(gulp.dest('../static/webfonts'));
});


gulp.task('default', ['sass', 'javascript', 'fonts'], function() {
  gulp.watch(['scss/**/*.scss'], ['sass']);
  gulp.watch(['../static/js/source.js'], ['javascript']);
});