from django.apps import AppConfig


class ToolchestConfig(AppConfig):
    name = 'toolchest'
