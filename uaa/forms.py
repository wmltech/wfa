from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import password_validation
from django.contrib.auth.password_validation import MinimumLengthValidator, UserAttributeSimilarityValidator, CommonPasswordValidator, NumericPasswordValidator
from uaa.models import User


class RegistrationForm(forms.Form):
    email = forms.EmailField(required=True)

    def clean_email(self):
        data = self.cleaned_data['email']
        if User.objects.filter(username=data):
            raise forms.ValidationError("Email already exists")
        return data

    def save(self, *args, **kwargs):
        email = self.cleaned_data.get('email')
        user = User.objects.create_user(username=email, email=email, password=None)
        return user


class CompleteRegistrationForm(forms.Form):
    first_name = forms.CharField(max_length=60,required=True)
    last_name = forms.CharField(max_length=60,required=True)
    password = forms.CharField( widget=forms.PasswordInput,
                                help_text=password_validation.password_validators_help_text_html(),)

    def create_user(self, *args, **kwargs):
        pass

    def clean_password(self):
        password = self.cleaned_data['password']
        if password:
            password_validation.validate_password(password)
        return password


"""
class AuthenticationForm(forms.Form):
    email = forms.EmailField(required=True)
    password = forms.CharField(strip=False, widget=forms.PasswordInput)


    

    def check_login(self):
        email = self.cleaned_data['email']
        password = self.cleaned_data['password']
        a = authenticate(username=email, password=password)
        type(a)
        print('a', a)


"""
class EditProfileForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name','last_name']