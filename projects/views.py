from django.shortcuts import render, Http404, redirect, get_object_or_404
from django import forms
from string import ascii_uppercase
import time
import datetime
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.decorators import login_required
from uaa.models import User
from openpyxl.worksheet.write_only import WriteOnlyCell
from organisations.models import Organisation
from teams.models import Team,TeamMember
from projects.models import Project, Section,Category,Severity, Task
from .forms import ProjectForm, ProjectSectionForm, ProjectTaskForm, TaskForm, TaskApprovalForm,EditParentTaskForm,EditGenricApprovalForm,DeleteSectionForm,DeleteProjectForm,DeleteTaskForm
from openpyxl import Workbook
import openpyxl
from openpyxl.styles import Font
from openpyxl.writer.excel import save_virtual_workbook
from django.core.exceptions import ValidationError
from django.core.paginator import Paginator


import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Create your views here.
@login_required
def project_home(request, slug, project_id):
    projects = Project.objects.filter(organisation__slug=slug, organisation__team__teammember__member=request.user).distinct().select_related('organisation')
    project = get_object_or_404(projects, id=project_id)

    tasks = Task.objects.filter(section__project=project, parent__isnull=True).order_by(
        'section', 'order'
    ).select_related(
        'section__project__organisation', 'category','severity','executed_by','assigned_to_team',
        'assigned_to_user', 'parent', 'created_by'
    ).prefetch_related('parenttask_set', 'note_set', 'attachment_set', 'parenttask_set__category',
                       'parenttask_set__assigned_to_user', 'parenttask_set__executed_by',
                       'parenttask_set__assigned_to_team', 'parenttask_set__created_by' )

    teams = Team.objects.filter(teammember__member=request.user ).select_related('organisation')
    print(teams)

    context = {'project': project, 'tasks': tasks, 'current_page': 'projects','teams':teams}

    return render(request, 'projects/project_home.html', context=context)

@login_required
def projects_download(request, slug):
    ids = request.GET.get('ids', []).strip(',')
    project_ids = ids.split(',')
    wb = project_report(project_ids)
    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y%m%d%H%M%S')
    file_name = 'TJ_ProjectReport' + '_' + str(st) + '.xlsx'
    content_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    response = HttpResponse(save_virtual_workbook(wb), content_type=content_type)
    response['Content-Disposition'] = 'attachment; filename=' + file_name
    return response

def project_report(project_ids):
    wb = Workbook()
    isFirst = True
    for project_id in project_ids:
        project = get_object_or_404(Project, id=project_id)
        if isFirst:
            ws = wb.active
            ws.title = 'PR' + str(project.name)
            isFirst = False
        else:
            ws = wb.create_sheet('PR' + str(project.name))

        src = os.path.join(BASE_DIR, 'static', 'templates', 'one', 'Avilon', 'img', '1.png')
        img = openpyxl.drawing.image.Image(src)

        ws.add_image(img, 'A1')
        ws.merge_cells('E1:F1')
        ws.merge_cells('E2:F2')
        ws.merge_cells('E3:F3')
        ws['E1'] = 'Name'
        ws['G1'] = project.name
        ws['E2'] = 'Reference'
        ws['G2'] = project.reference
        ws['E3'] = 'Description'
        ws['G3'] = project.description
        ws['E1'].font = Font(bold=True)
        ws['E2'].font = Font(bold=True)
        ws['E3'].font = Font(bold=True)

        row = 6
        sections = Section.objects.filter(project=project)
        for section in sections:
            tasks = Task.objects.filter(section=section, parent__isnull=True)
            task_count = tasks.count()
            print(task_count)
            for column in ascii_uppercase:
                if column == 'K' or column == 'L' or column == 'B' or column == 'C' or column == 'G' or column == 'M' or column == 'N':
                    ws.column_dimensions[column].width = 20
            if tasks:
                ws.cell(row=row, column=1).value = (section.name)
                for i in range(1, 3):
                    ws.cell(row=row, column=i).font = Font(bold=True)
                row = row + 1
                start_row = row
                a = ['Task ID', 'Task Name', 'Parent', 'Task Reference', 'Task Type', 'Status', 'Team Assigned',
                     'User Assigned', 'Approval Status', 'Completed By', 'Approval By', 'Requested Date',
                     'Completed Date/Approved Date']
                for i in range(0, 13):
                    ws.cell(row=row, column=i + 1).value = a[i]

                for i in range(1, 14):
                    ws.cell(row=row, column=i).font = Font(bold=True)
                row = row + task_count + 2

            for task in tasks:
                if task.type == 'P':
                    parent_data = list()
                    parent_data.append(task.id)
                    parent_data.append(task.name)
                    parent_data.append('')
                    parent_data.append('')
                    parent_data.append('PARENT')
                    """parent_data.append(task.status)"""
                    if task.status == 'SUCCESS':
                        parent_data.append('Completed')
                    elif task.status == 'PENDING':
                        parent_data.append('Pending')
                    elif task.status == 'FAILURE':
                        parent_data.append('Rejected')
                    elif task.status == 'STARTED':
                        parent_data.append('In Progress')
                    elif task.status == 'REJECTED':
                        parent_data.append('Rejected')
                    else:
                        parent_data.append('Unknown')
                    ws.append(parent_data)

                    for child_task in task.parenttask_set.all():
                        row = row + 1
                        print(row)
                        child_data = list()
                        child_data.append(child_task.id)
                        child_data.append(child_task.name)
                        child_data.append(task.name)
                        child_data.append(child_task.reference)
                        if child_task.type == 'G':
                            child_data.append('GENERIC')
                        else:
                            child_data.append('APPROVAL')
                        """child_data.append(child_task.status)"""
                        if child_task.status == 'SUCCESS':
                            if child_task.type == 'G':
                                child_data.append('Completed')
                            elif child_task.type == 'A':
                                child_data.append('Approved')
                            elif child_task.type == 'P':
                                child_data.append('Completed')
                        elif child_task.status == 'PENDING':
                            child_data.append('Pending')
                        elif child_task.status == 'FAILURE':
                            child_data.append('Rejected')
                        elif child_task.status == 'STARTED':
                            child_data.append('In Progress')
                        elif child_task.status == 'REJECTED':
                            child_data.append('Rejected')
                        else:
                            child_data.append('Unknown')
                        if child_task.assigned_to_team:
                            child_data.append(child_task.assigned_to_team.name)
                        else:
                            child_data.append(child_task.assigned_to_team)

                        if child_task.assigned_to_user:
                            child_data.append(child_task.assigned_to_user.get_full_name())
                        else:
                            child_data.append(child_task.assigned_to_user)
                        if child_task.approval_status:
                            child_data.append(child_task.get_approval_status_display())
                        else:
                            child_data.append('')
                        if child_task.type == 'G' and child_task.executed_by:
                            child_data.append(child_task.executed_by.get_full_name())
                        else:
                            child_data.append('')
                        if child_task.type == 'A' and child_task.executed_by:
                            child_data.append(child_task.executed_by.get_full_name())
                        else:
                            child_data.append('')
                        if child_task.target_start_datetime:
                            child_data.append(child_task.target_start_datetime)
                        if child_task.status == 'SUCCESS' or child_task.status == 'FAILURE':
                            child_data.append(child_task.executed_timestamp)
                        ws.append(child_data)

                else:
                    data = list()
                    data.append(task.id)
                    data.append(task.name)
                    data.append('')
                    data.append(task.reference)
                    if task.type == 'G':
                        data.append('GENERIC')
                    else:
                        data.append('APPROVAL')
                    if task.status == 'SUCCESS':
                        if task.type == 'G':
                            data.append('Completed')
                        elif task.type == 'A':
                            data.append('Approved')
                        elif task.type == 'P':
                            data.append('Completed')
                    elif task.status == 'PENDING':
                        data.append('Pending')
                    elif task.status == 'FAILURE':
                        data.append('Rejected')
                    elif task.status == 'STARTED':
                        data.append('In Progress')
                    elif task.status == 'REJECTED':
                        data.append('Rejected')
                    else:
                        data.append('Unknown')
                    """data.append(task.status)"""
                    if task.assigned_to_team:
                        data.append(task.assigned_to_team.name)
                    else:
                        data.append(task.assigned_to_team)
                    if task.assigned_to_user:
                        data.append(task.assigned_to_user.get_full_name())
                    else:
                        data.append(task.assigned_to_user)
                    if task.approval_status:
                        data.append(task.get_approval_status_display())
                    else:
                        data.append('')
                    if task.type == 'G' and task.executed_by:
                        data.append(task.executed_by.get_full_name())
                    else:
                        data.append('')
                    if task.type == 'A' and task.executed_by:
                        data.append(task.executed_by.get_full_name())
                    else:
                        data.append('')
                    if task.target_start_datetime:
                        data.append(task.target_start_datetime)
                    if task.status == 'SUCCESS' or task.status == 'FAILURE':
                        data.append(task.executed_timestamp)
                    ws.append(data)
            end_row = row - 2
            table_starting_row = 'A' + str(start_row)
            table_end_row = 'M' + str(end_row)
            table_start_end = table_starting_row + ':' + table_end_row
            ws.sheet_properties.pageSetUpPr.fitToPage = True
            ws.page_setup.fitToHeight = False
            ws.sheet_view.showGridLines = False
            medium_style = openpyxl.worksheet.table.TableStyleInfo(name="TableStyleMedium9",
                                                                   showRowStripes=False, showColumnStripes=False)
            # create a table
            table_name = 'sectionTable' + str(row)
            table = openpyxl.worksheet.table.Table(ref=table_start_end, displayName=table_name,
                                                   tableStyleInfo=medium_style)
            # add the table to the worksheet
            ws.add_table(table)
    return wb


@login_required
def project_download(request, slug, project_id):
    print("project")
    projects = [project_id, ]
    wb = project_report(projects)

    ts = time.time()
    st = datetime.datetime.fromtimestamp(ts).strftime('%Y%m%d%H%M%S')
    file_name = 'TJ_PROJECT' + str(project_id) + '_' + str(st) + '.xlsx'
    content_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    response = HttpResponse(save_virtual_workbook(wb), content_type=content_type)
    response['Content-Disposition'] = 'attachment; filename=' + file_name
    return response


@login_required
def add_project(request, slug):
    organisations = Organisation.objects.filter(
        slug=slug, team__teammember__member=request.user, active=True
    ).distinct()
    if len(organisations) != 1:
        raise Http404
    organisation = organisations.first()
    form = ProjectForm(request.POST or None)
    form.fields['organisation'] = forms.ModelChoiceField(Organisation.objects.filter(id=organisation.id))
    form.fields['organisation'].widget = forms.HiddenInput()
    form.fields['organisation'].initial = organisation
    if form.is_valid():
        project = form.save(commit=False)
        project.created_by = request.user
        project.modified_by = request.user
        project.save()
        return redirect('project_home', slug=organisation.slug, project_id=project.id)

    context = {'organisation': organisation, 'form': form}
    return render(request, 'projects/add_project.html', context=context)

@login_required
def change_project(request, slug, project_id):
    organisations = Organisation.objects.filter(
        slug=slug, team__teammember__member=request.user, active=True
    ).distinct()
    if len(organisations) != 1:
        raise Http404
    organisation = organisations.first()
    project = Project.objects.get(id=project_id)
    form = ProjectForm(request.POST or None, instance=project)
    form.fields['organisation'] = forms.ModelChoiceField(Organisation.objects.filter(id=organisation.id))
    form.fields['organisation'].widget = forms.HiddenInput()
    form.fields['organisation'].initial = organisation
    if form.is_valid():
        project = form.save(commit=False)
        project.modified_by = request.user
        project.save()
        return redirect('project_home', slug=organisation.slug, project_id=project_id)
    context = {'form': form,'project':project}
    return render(request, 'projects/add_project.html', context=context)

@login_required
def delete_project(request, slug,project_id):
    organisations = Organisation.objects.filter(
        slug=slug, team__teammember__member=request.user, active=True
    ).distinct()
    if len(organisations) != 1:
        raise Http404
    organisation = organisations.first()
    form = DeleteProjectForm(request.POST or None)
    if form.is_valid():
        Project.objects.get(id=project_id).delete()
        return redirect('my_tasks',slug=slug)
    context = {'form': form}
    return render(request, 'projects/delete_project.html', context=context)



@login_required
def change_section(request, slug,project_id,section_id):
    organisations = Organisation.objects.filter(
        slug=slug, team__teammember__member=request.user, active=True
    ).distinct()
    if len(organisations) != 1:
        raise Http404
    organisation = organisations.first()
    section = Section.objects.get(id=section_id)
    form = ProjectSectionForm(request.POST or None, instance=section)
    form.fields['organisation'] = forms.ModelChoiceField(Organisation.objects.filter(id=organisation.id))
    form.fields['organisation'].widget = forms.HiddenInput()
    form.fields['organisation'].initial = organisation
    if form.is_valid():
        section = form.save(commit=False)
        section.modified_by = request.user
        section.save()
        return redirect('project_home', slug=organisation.slug, project_id=project_id)
    context = {'form': form}
    return render(request, 'projects/change_section.html', context=context)

@login_required
def delete_section(request, slug,project_id,section_id):
    organisations = Organisation.objects.filter(
        slug=slug, team__teammember__member=request.user, active=True
    ).distinct()
    if len(organisations) != 1:
        raise Http404
    organisation = organisations.first()
    form = DeleteSectionForm(request.POST or None)
    if form.is_valid():
        Section.objects.get(id=section_id).delete()
        return redirect('project_home', slug=organisation.slug, project_id=project_id)
    context = {'form': form}
    return render(request, 'projects/delete_section.html', context=context)

@login_required
def get_project_sections(request, slug, project_id):
    organisation = get_object_or_404(Organisation, slug=slug)
    project = get_object_or_404(Project, id=project_id, organisation=organisation)
    sections = Section.objects.filter(project=project)
    results = [{'id': x.id, 'text': x.name} for x in sections]
    return JsonResponse({'results': results})

@login_required
def get_project_categories(request, slug, project_id):
    organisation = get_object_or_404(Organisation, slug=slug)
    project = get_object_or_404(Project, id=project_id, organisation=organisation)
    categories = Category.objects.filter(project=project)
    results = [{'id': x.id, 'text': x.name} for x in categories]
    return JsonResponse({'results': results})

@login_required
def get_project_severities(request, slug, project_id):
    organisation = get_object_or_404(Organisation, slug=slug)
    project = get_object_or_404(Project, id=project_id, organisation=organisation)
    severities = Severity.objects.filter(project=project)
    results = [{'id': x.id, 'text': x.name} for x in severities]
    return JsonResponse({'results': results})

@login_required
def get_project_tasks(request, slug, project_id):
    organisation = get_object_or_404(Organisation, slug=slug)
    project = get_object_or_404(Project, id=project_id, organisation=organisation)
    tasks = Task.objects.filter(section__project=project).exclude(type='P').order_by('section','id')
    results = [{'id': x.id, 'text': x.name} for x in tasks]

    return JsonResponse({'results': results})

@login_required
def add_task(request, slug, project_id, task_type, parent_id=None):
    organisation = get_object_or_404(Organisation, slug=slug)
    project = get_object_or_404(Project, id=project_id, organisation=organisation)

    parent = None
    if parent_id:
        parent = get_object_or_404(Task, id=parent_id, section__project=project)

    form = TaskForm(request.POST or None, project=project, task_type=task_type, parent=parent)

    if form.is_valid():
        form.save(user=request.user)
        return redirect('project_home', slug=organisation.slug, project_id=project.id)
    context = {'organisation': organisation, 'project': project, 'form': form, 'task_type': task_type }

    return render(request, 'projects/add_task.html', context=context)

@login_required
def task_home(request, slug, project_id, task_id):
    organisation = Organisation.objects.get(slug=slug)
    project = Project.objects.get(id=project_id)
    task = Task.objects.get(id=task_id, section__project=project, parent__isnull=True)

    context = {'organisation': organisation, 'project': project, 'task': task}
    return render(request, 'projects/task_home.html', context=context)

@login_required
def my_tasks(request, slug):
    organisations = Organisation.objects.filter(
        slug=slug, team__teammember__member=request.user, active=True
    ).distinct()
    if len(organisations) != 1:
        raise Http404
    organisation = organisations.first()
    print(organisation)
    query = request.GET.get('status', '')
    teams = Team.objects.filter(organisation__slug=slug, organisation__team__teammember__member=request.user).distinct()
    if query:
        tasks = Task.objects.filter( section__project__organisation=organisation,assigned_to_user_id=request.user.id,status=query).order_by('-id').select_related('section__project__organisation')
    else:
        tasks = Task.objects.filter( section__project__organisation=organisation,assigned_to_user_id=request.user.id).order_by('-id').select_related('section__project__organisation')
        print(tasks.count())
    """
    paginator = Paginator(tasks, 25)
    page = request.GET.get('page', '')
    tasks = paginator.get_page(page)
    """
    context = {'tasks': tasks , 'current_page': 'tasks','organisation': organisation,'status':query}
    return render(request, 'projects/my_tasks.html', context=context)

@login_required
def my_team_tasks(request,slug):
    organisations = Organisation.objects.filter(
        slug=slug, team__teammember__member=request.user, active=True
    ).distinct()
    if len(organisations) != 1:
        raise Http404
    organisation = organisations.first()
    query = request.GET.get('status', '')
    teams = Team.objects.filter(organisation__slug=slug, organisation__team__teammember__member=request.user).distinct()
    if query:
        tasks = Task.objects.filter(assigned_to_team__in=teams,status=query).select_related('section__project__organisation')
    else:
        tasks = Task.objects.filter(assigned_to_team__in=teams).select_related('section__project__organisation')
    """
    paginator = Paginator(tasks, 15)
    page = request.GET.get('page', '')
    tasks = paginator.get_page(page)
    """
    context = {'tasks': tasks,'current_page': 'tasks','organisation': organisation,'status':query }
    return render(request, 'projects/my_team_tasks.html', context=context)

@login_required
def edit_task(request, slug,project_id,task_id):
    organisation = get_object_or_404(Organisation, slug=slug)
    project = get_object_or_404(Project, id=project_id, organisation=organisation)
    task = Task.objects.get(id=task_id, section__project=project)
    task_type = task.get_type_display().lower()
    if task.status == 'PENDING':
        form = TaskForm(
            request.POST or None, project=task.section.project, task_type=task_type, parent=task.parent, instance=task
        )
    else:
        return render(request, 'projects/edit_task_error.html', context=None)

        raise Http404('You can update only pending tasks')
    if form.is_valid():
        task = form.save(user=request.user)
        task.modified_by = request.user

    context = {'organisation': organisation, 'project': project,'form': form,'task':task, 'task_type': task_type}
    return render(request, 'projects/add_task.html', context=context)


@login_required
def delete_task(request, slug,project_id,task_id):
    organisations = Organisation.objects.filter(
        slug=slug, team__teammember__member=request.user, active=True
    ).distinct()
    if len(organisations) != 1:
        raise Http404
    organisation = organisations.first()
    form = DeleteTaskForm(request.POST or None)
    if form.is_valid():
        Task.objects.get(id=task_id).delete()
        return redirect('project_home', slug=organisation.slug, project_id=project_id)
    context = {'form': form}
    return render(request, 'projects/delete_task.html', context=context)

@login_required
def clone_task(request, slug,project_id,task_id):
    print(task_id)
    organisation = get_object_or_404(Organisation, slug=slug)
    project = get_object_or_404(Project, id=project_id, organisation=organisation)

    task = Task.objects.get(id=task_id,section__project = project)
    task_type = task.get_type_display().lower()
    form = TaskForm(request.POST or None, project=task.section.project, task_type=task_type, parent=task.parent, instance=task)

    if form.is_valid():
        task=form.clone(user=request.user)
        task.modified_by = request.user

    context = {'organisation': organisation, 'project': project,'form': form,'task':task, 'task_type': task_type}
    return render(request, 'projects/add_task.html', context=context)
