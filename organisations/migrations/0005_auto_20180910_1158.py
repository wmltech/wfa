# Generated by Django 2.1 on 2018-09-10 11:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('organisations', '0004_auto_20180905_1211'),
    ]

    operations = [
        migrations.AlterField(
            model_name='organisation',
            name='deleted',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
