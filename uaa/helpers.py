from django.core import signing
from django.conf import settings
from django.template.loader import render_to_string,get_template


def get_base_url(request):
    """
    :param request: Takes HTTP request as input to evaluate base url
    :return: Site URL (base) either from settings or from request.
    """
    if hasattr(settings, 'BASE_URL'):
        base_url = settings.BASE_URL
    else:
        base_url = '{}://{}'.format(request.scheme, request.get_host())
    return base_url


def send_user_creation_email(user, base_url, team=None):
    """
    :param user: This is django user instance
    :param base_url: This is URL for the website so it will be prepended to the link
    :return: None (Sends user email with link to activate his/her account)
    """
    activation_key = signing.dumps(user.username, 'uaa')
    context = {'user': user, 'activation_key': activation_key, 'base_url': base_url, 'team': team}

    if team:
        email_subject = render_to_string(template_name='uaa/emails/register_via_team_email_subject.txt', context=context)
        email_body = render_to_string(template_name='uaa/emails/register_via_team_email_body.txt', context=context)
        #message = get_template('uaa/emails/register_user_via_team_email.html').render(context)

    else:
        email_subject = render_to_string(template_name='uaa/emails/register_email_subject.txt', context=context)
        email_body = render_to_string(template_name='uaa/emails/register_email_body.txt', context=context)
        #message = get_template('uaa/emails/register_email_template.html').render(context)


    #user.email_user(subject=email_subject, message=email_body)
    user.email_user(subject=email_subject, message=email_body, html_message=email_body)