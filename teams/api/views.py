from rest_framework import viewsets, generics
from .serializers import TeamsSerializer,TeamMembersSerializer, TeamMemberSerialiser, UserSerializerWithFullName,MakeAdminTeamMemberSerialiser,RemoveAdminTeamMemberSerialiser, TeamMemberChoiceSerializer
from teams.models import Team,TeamMember
from uaa.models import User
from rest_framework.response import Response
from rest_framework import status
from organisations.api.permissions import OrganisationPermission


class TeamViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Team.objects.all().order_by('-id')
    serializer_class = TeamsSerializer


class TeamMemberViewSet(viewsets.ModelViewSet):
    queryset = TeamMember.objects.all()

    def get_queryset(self):
        """
        Optionally restricts the returned purchases to a given user,
        by filtering against a `username` query parameter in the URL.
        """
        team_slug = self.request.query_params.get('team_slug', None)
        organisation_slug = self.request.query_params.get('organisation_slug', None)
        queryset = TeamMember.objects.filter(team__slug=team_slug, team__organisation__slug=organisation_slug)
        return queryset

    serializer_class = TeamMembersSerializer


class AddTeamMemberView(generics.GenericAPIView):
    serializer_class = TeamMemberSerialiser

    def post(self, request, organisation_id, team_id):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        from django.utils import timezone
        print('A', timezone.now())
        user = serializer.add(request, organisation_id, team_id)
        print(user)
        print('B', timezone.now())
        serializer = UserSerializerWithFullName({
            'id': user.id,
            'username': user.username,
            'email': user.email,
            'full_name': user.get_full_name()
        })
        return Response(serializer.data, status=status.HTTP_201_CREATED)



class RemoveTeamMemberView(generics.GenericAPIView):
    serializer_class = TeamMemberSerialiser

    def post(self, request, organisation_id, team_id):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.remove(request,organisation_id, team_id)
        return Response({}, status=status.HTTP_201_CREATED)

class MakeAdminTeamMemberView(generics.GenericAPIView):
    permission_classes = (OrganisationPermission,)
    serializer_class = MakeAdminTeamMemberSerialiser

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.make_admin()
        return Response({}, status=status.HTTP_201_CREATED)

class RemoveAdminTeamMemberView(generics.GenericAPIView):
    permission_classes = (OrganisationPermission,)
    serializer_class = RemoveAdminTeamMemberSerialiser

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.remove_admin()
        return Response({}, status=status.HTTP_201_CREATED)


class TeamUserView(generics.ListAPIView):
    serializer_class = TeamMemberChoiceSerializer

    def get_queryset(self,):
        team_id = self.request.GET.get('team_id', None)
        filters = {'teammember__team': team_id,}
        query_set = User.objects.filter(**filters).select_related()
        return query_set