from rest_framework import viewsets
from .serializers import OrganisationSerializer
from organisations.models import Organisation
from uaa.models import User
from rest_framework.views import APIView
from rest_framework.response import Response
from .permissions import OrganisationPermission

class OrganisationViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Organisation.objects.all().order_by('-id')
    serializer_class = OrganisationSerializer

class OrganisationMembersView(APIView):
    permission_classes = (OrganisationPermission,)

    def get(self, request, org_id):
        members = User.objects.filter(teammember__team__organisation=org_id)
        usernames = [{'id': user.id, 'username': user.username, 'email': user.email, 'name': user.get_full_name()} for user in members]
        return Response(usernames)