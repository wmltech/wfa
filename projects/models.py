import os
from django.conf import settings
from django.db import models
from organisations.models import Organisation
from teams.models import Team
from uuid import uuid4


# Create your models here.


"""
PROJECT_STATUS_CHOICES = (('P', 'Pending'), ('S', 'Started'), ('C', 'Complete'))


class Project(models.Model):
    name = models.CharField(max_length=100)
    organisation = models.ForeignKey(Organisation, on_delete=models.CASCADE)
    reference = models.CharField(max_length=50, blank=True)
    description = models.TextField(max_length=2000, blank=True)
    status = models.CharField(max_length=1, choices=PROJECT_STATUS_CHOICES, default='P')

    # Additional Info
    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='project_created_by', on_delete=models.CASCADE)
    modified = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='project_modified_by', on_delete=models.CASCADE)

    def __str__(self):
        return '{} -> {}'.format(self.organisation, self.name)


class ProjectSection(models.Model):
    name = models.CharField(max_length=60)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    order = models.PositiveIntegerField(default=0)

    # Additional Info
    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='section_created_by', on_delete=models.CASCADE)
    modified = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='section_modified_by', on_delete=models.CASCADE)

    def __str__(self):
        return '{} -> {}'.format(self.name, self.project)

    class Meta:
        unique_together = ('name', 'project')


PROJECT_TASK_TYPE_CHOICES = (('P', 'Parent'), ('A', 'Approval'), ('G', 'Generic'))
PROJECT_TASK_STATUS_CHOICES = (('P', 'Pending'), ('I', 'In Progress'), ('C', 'Complete'))


class ProjectTask(models.Model):
    name = models.CharField(max_length=60)
    order = models.PositiveIntegerField(default=0)
    description = models.TextField(max_length=2000, blank=True)
    section = models.ForeignKey(ProjectSection, on_delete=models.CASCADE)
    task_type = models.CharField(max_length=1, choices=PROJECT_TASK_TYPE_CHOICES)
    status = models.CharField(max_length=1, choices=PROJECT_TASK_STATUS_CHOICES)
    parent = models.ForeignKey('self', related_name= 'task_parent', blank=True, null=True, on_delete=models.CASCADE)
    dependent_on = models.ForeignKey('self',    related_name= 'task_dependent_on', blank=True, null=True, on_delete=models.CASCADE)
    start_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)
    # TODO: Use actual_start_date and actual_end_date (and rename above fields to target_start_date and target_end_date

    # Assignment Info
    assigned_to_team = models.ForeignKey(Team, blank=True, null=True, on_delete=models.CASCADE)
    assigned_to_user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='task_assigned_to_user', blank=True, null=True, on_delete=models.CASCADE)
    assigned_by = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, related_name='task_assigned_to_by', null=True, on_delete=models.CASCADE)
    assigned_time = models.DateTimeField(blank=True, null=True)

    # Additional Info
    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='task_created_by', on_delete=models.CASCADE)
    modified = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='task_modified_by', on_delete=models.CASCADE)

    def __str__(self):
        return '{} - {}'.format(self.id, self.name)





PROJECT_TASK_WORK_TYPE_CHOICES = (('A', 'Approval'), ('G', 'Generic'))



class ProjectTaskWork(models.Model):
    ""/"
    A Task can have multiple work tasks  (this is like a sub-tasks)
    ""/"
    task = models.ForeignKey(ProjectTask, on_delete=models.CASCADE)
    description = models.TextField(blank=True)
    work_type = models.CharField(max_length=1, choices=PROJECT_TASK_WORK_TYPE_CHOICES)
    notes = models.TextField(blank=True)

    # Approval Type Related Fields
    approved = models.BooleanField(blank=True, null=True)
    approved_by = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, null=True, on_delete=models.CASCADE)

    # Generic Type Related Fields
    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='taskwork_created_by',
                                   on_delete=models.CASCADE)
    modified = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='taskwork_modified_by',
                                    on_delete=models.CASCADE)

"""


PROJECT_STATUS_CHOICES = (('PENDING', 'Pending'), ('STARTED', 'Started'), ('SUCCESS', 'Success'), ('FAILURE', 'Failure'))


class Project(models.Model):
    name = models.CharField(max_length=100)
    organisation = models.ForeignKey(Organisation, on_delete=models.CASCADE)
    reference = models.CharField(max_length=50, blank=True)
    description = models.TextField(max_length=2000, blank=True)
    status = models.CharField(max_length=10, choices=PROJECT_STATUS_CHOICES, default='PENDING')

    # Additional Info
    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='project_created_by', on_delete=models.CASCADE)
    modified = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='project_modified_by', on_delete=models.CASCADE)

    def __str__(self):
        return '{} -> {}'.format(self.organisation, self.name)


class Section(models.Model):
    name = models.CharField(max_length=60)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    order = models.PositiveIntegerField(default=0)

    # Additional Info
    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='createdsection_set', on_delete=models.CASCADE)
    modified = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='modifiedsection_set', on_delete=models.CASCADE)

    def __str__(self):
        return '{} -> {}'.format(self.name, self.project)

    class Meta:
        unique_together = ('name', 'project')
        ordering = ('order',)


class Category(models.Model):
    name = models.CharField(max_length=60)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    order = models.PositiveIntegerField(default=0)

    # Additional Info
    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='createdcategory_set', on_delete=models.CASCADE)
    modified = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='modifiedcategory_set', on_delete=models.CASCADE)

    def __str__(self):
        return '{} -> {}'.format(self.name, self.project)

    class Meta:
        unique_together = ('name', 'project')
        ordering = ('order',)

class Severity(models.Model):
    name = models.CharField(max_length=60)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    order = models.PositiveIntegerField(default=0)

    # Additional Info
    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='createdseverity_set', on_delete=models.CASCADE)
    modified = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='modifiedseverity_set', on_delete=models.CASCADE)

    def __str__(self):
        return '{} -> {}'.format(self.name, self.project)

    class Meta:
        unique_together = ('name', 'project')
        ordering = ('order',)


TASK_TYPE_CHOICES = (('P', 'Parent'), ('A', 'Approval'), ('G', 'Generic'))
TASK_STATUS_CHOICES = (('PENDING', 'Pending'), ('STARTED', 'Started'), ('SUCCESS', 'Success'), ('FAILURE', 'Failure'))
#TASK_SEVERITY_CHOICES = (('HIGH', 'High'), ('MEDIUM', 'Medium'), ('Low', 'Low'))


class Task(models.Model):
    """
    Projects are made of tasks
    """
    name = models.CharField(max_length=100)
    section = models.ForeignKey(Section, on_delete=models.CASCADE)
    category = models.ForeignKey(Category,blank=True, null=True, on_delete=models.CASCADE)
    severity = models.ForeignKey(Severity,blank=True, null=True, on_delete=models.CASCADE)
    type = models.CharField(max_length=1, choices=TASK_TYPE_CHOICES)
    parent = models.ForeignKey('self', related_name='parenttask_set', blank=True, null=True, on_delete=models.CASCADE)
    status = models.CharField(max_length=10, choices=TASK_STATUS_CHOICES)
    dependent_on_section = models.ForeignKey(Section, related_name='dependentsectiontask_set', blank=True, null=True, on_delete=models.CASCADE)
    dependent_on_task = models.ForeignKey('self', related_name='dependenttask_set', blank=True, null=True, on_delete=models.CASCADE)
    duration = models.PositiveIntegerField(default=3, blank=True, null=True,)
    order = models.PositiveIntegerField(default=0)
    reference = models.CharField(max_length=50, blank=True)

    # Capture Dates
    target_start_datetime = models.DateTimeField(blank=True, null=True)
    target_end_datetime = models.DateTimeField(blank=True, null=True)
    actual_start_datetime = models.DateTimeField(blank=True, null=True)
    actual_end_datetime = models.DateTimeField(blank=True, null=True)

    # Assignment Info
    assigned_to_team = models.ForeignKey(Team, blank=True, null=True, on_delete=models.CASCADE)
    assigned_to_user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='taskassigned_to_set', blank=True,
                                         null=True, on_delete=models.CASCADE)
    assigned_by = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, related_name='taskassigned_by_set', null=True,
                                    on_delete=models.CASCADE)
    assigned_time = models.DateTimeField(blank=True, null=True)

    executed_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='executedtask_set', on_delete=models.CASCADE,
                                    blank=True, null=True)
    executed_timestamp = models.DateTimeField(blank=True, null=True)
    notes = models.TextField(max_length=2000, blank=True)

    # Approval Task
    approval_status = models.CharField(max_length=1, choices=(('A', 'Approved'), ('R', 'Rejected')), blank=True)
    #approval_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='approvaltask_set', on_delete=models.CASCADE,
    #                                blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='createdtask_set', on_delete=models.CASCADE)
    modified = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='modifiedtask_set', on_delete=models.CASCADE)

    def __str__(self):
        return '{} -> {}'.format(self.section.name, self.name)

    class Meta:
        ordering = ('order', )


class Note(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    notes = models.TextField(max_length=2000)

    # Additional Info
    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='creatednote_set', on_delete=models.CASCADE)
    modified = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='modifiednote_set', on_delete=models.CASCADE)


def task_attachment_path(instance, filename):
    extension = os.path.splitext(filename)[1]
    path = '{}/tasks/attachments/{}/{}/{}'.format(
        instance.task.section.project.organisation.id, instance.task.section.project.id,
        instance.task.id, uuid4().hex[:8])
    if extension:
        path = '{}{}'.format(path, extension)
    return path


class Attachment(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    file = models.FileField(upload_to=task_attachment_path)
    name = models.CharField(max_length=100, blank=True)
    description = models.TextField(max_length=2000, blank=True)

    # Additional Info
    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='task_attachment_created_by',
                                   on_delete=models.CASCADE)
    modified = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='task_attachment_modified_by',
                                    on_delete=models.CASCADE)

def project_task_attachment_path(instance, filename):
    extension = os.path.splitext(filename)[1]
    path = '{}/tasks/attachments/{}/{}/{}'.format(
        instance.task.section.project.organisation.id, instance.task.section.project.id,
        instance.task.id, uuid4().hex[:8])
    if extension:
        path = '{}{}'.format(path, extension)
    return path


"""
class Log(models.Model):
    pass


class ProjectTaskAttachment(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    file = models.FileField(upload_to=project_task_attachment_path)
    name = models.CharField(max_length=100)
    description = models.TextField(max_length=2000, blank=True)

    # Additional Info
    created = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='task_attachment_created_by', on_delete=models.CASCADE)
    modified = models.DateTimeField(auto_now=True)
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='task_attachment_modified_by', on_delete=models.CASCADE)




WORK_TYPE_CHOICES = (('G', 'Generic'), ('A', 'Approval'))
WORK_STATUS_CHOICES = (('W', 'Waiting'), ('P', 'In-Progress'), ('C', 'Completed'))


class Work(models.Model):
    "/""
    Tasks are made of work units
    "/""
    name = models.CharField(max_length=100)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    type = models.CharField(max_length=1, choices=WORK_TYPE_CHOICES)
    order = models.PositiveIntegerField(default=0)
    notes = models.TextField(max_length=2000, blank=True)
    status = models.CharField(max_length=1, choices=WORK_STATUS_CHOICES)


    # Assignment Info
    assigned_to_team = models.ForeignKey(Team, blank=True, null=True, on_delete=models.CASCADE)
    assigned_to_user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='work_assigned_to_user', blank=True,
                                         null=True, on_delete=models.CASCADE)
    assigned_by = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, related_name='work_assigned_to_by', null=True,
                                    on_delete=models.CASCADE)
    assigned_time = models.DateTimeField(blank=True, null=True)

"""











