from django.apps import AppConfig


class UaaConfig(AppConfig):
    name = 'uaa'
