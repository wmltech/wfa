from openpyxl import Workbook
wb = Workbook()

# grab the active worksheet
ws = wb.active

# Data can be assigned directly to cells

ws.merge_cells('A1:B1')
ws.merge_cells('A2:B2')
ws.merge_cells('A3:B3')
ws['A1'] = 'Project Name'
ws['C1'] = 'Dev'
ws['A2'] = 'Project reference'
ws['A3'] = 'Project Description'
ws['A5'] = 'Tasks'
ws['A6'] = 'Task A parent'
ws['B7'] = 'SubTasks'
ws['B8'] = 'TaskName'
ws['C8'] = 'Task Status'
ws['B9'] = 'Task A'
ws['C9'] = 'Pending'
# Python types will automatically be converted

# Save the file
wb.save("sample.xlsx")