from PyPDF2 import PdfFileWriter, PdfFileReader
import datetime
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import Paragraph, Table, TableStyle
from reportlab.lib.enums import TA_JUSTIFY, TA_LEFT, TA_CENTER
from reportlab.lib import colors


def coord(height, x, y, unit=1):
    x, y = x * unit, height -  y * unit
    return x, y


def addpage():
    width, height = A4
    styles = getSampleStyleSheet()
    styleN = styles["BodyText"]
    styleN.alignment = TA_LEFT
    styleBH = styles["Normal"]
    styleBH.alignment = TA_CENTER

    # Headers
    hdescrpcion = Paragraph('''<b>id</b>''', styleBH)
    hpartida = Paragraph('''<b>Name</b>''', styleBH)
    hcandidad = Paragraph('''<b>Timestamp</b>''', styleBH)
    hprecio_unitario = Paragraph('''<b>precio_unitario</b>''', styleBH)
    hprecio_total = Paragraph('''<b>precio_total</b>''', styleBH)

    # Texts
    now = datetime.datetime.now()
    time = ((str(datetime.datetime.now())).split('.')[0]).split(' ')[1] + ' ' + ((str(datetime.datetime.now())).split('.')[0]).split(' ')[0]

    descrpcion = Paragraph(time, styleN)
    partida = Paragraph('1', styleN)
    candidad = Paragraph('Meghala', styleN)
    precio_unitario = Paragraph('$52.00', styleN)
    precio_total = Paragraph('$6240.00', styleN)

    data= [[hdescrpcion, hcandidad,hcandidad, hprecio_unitario, hprecio_total],
           [partida, candidad, descrpcion, precio_unitario, precio_total]]

    table = Table(data, colWidths=[2.05 * cm, 2.7 * cm, 5 * cm,
                                   3* cm, 3 * cm])

    table.setStyle(TableStyle([
                           ('INNERGRID', (0,0), (-1,-1), 0.25, colors.black),
                           ('BOX', (0,0), (-1,-1), 0.25, colors.black),
                           ]))


    c = canvas.Canvas('watermark.pdf')
    table.wrapOn(c, width, height)
    table.drawOn(c, *coord(height, 1.8, 9.6, cm))
    c.save()
    pdf_file = PdfFileReader(open('watermark.pdf', 'rb'))
    count1 = pdf_file.getNumPages()
    # read_pdf = PdfFileReader(pdf_file)
    output = PdfFileWriter()
    for page_number in range(count1):
        print("Watermarking page {} of {}".format(page_number, count1))

        output.addPage(pdf_file.getPage(page_number))


    # finally, write "output" to document-output.pdf
    with open("FinalOutput.pdf", "wb") as outputStream:

        output.write(outputStream)
    pdfTwo = PdfFileReader(open("rr.pdf", "rb"))
    tt = pdfTwo.getNumPages()
    print(tt)
    for page_number in range(tt):
        print("Watermarking page {} of {}".format(page_number, tt))

        output.addPage(pdfTwo.getPage(page_number))


    # finally, write "output" to document-output.pdf
    with open("FinalOutput.pdf", "wb") as outputStream:

        output.write(outputStream)


if __name__ == '__main__':
    addpage()
