from django.utils import timezone
from rest_framework import serializers
from projects.models import Section, Task, Note
from teams.models import Team
from projects.helpers import email_task_owners



class SectionOrderSerializer(serializers.Serializer):
    section = serializers.IntegerField()
    order = serializers.ChoiceField(choices=[1,-1])

    def save(self, **kwargs):
        section = Section.objects.get(id=self.data.get('section'))
        order = self.data.get('order')
        section.order = section.order + order
        user = self.context['request'].user
        section.modified_by = user
        section.save()


class TaskOrderSerializer(serializers.Serializer):
    task = serializers.IntegerField()
    order = serializers.ChoiceField(choices=[1,-1])

    def save(self, **kwargs):
        task = Task.objects.get(id=self.data.get('task'))
        order = self.data.get('order')
        task.order = task.order + order
        user = self.context['request'].user
        task.modified_by = user
        task.save()


def update_parent_task_status(id):
    parent_task = Task.objects.get(id=id)
    if Task.objects.filter(parent=parent_task, status__in=['FAILURE', 'REJECTED']):
        parent_task.status = 'FAILURE'
    elif Task.objects.filter(parent=parent_task, status__in=['STARTED',]):
        parent_task.status = 'STARTED'
    elif Task.objects.filter(parent=parent_task, status__in=['PENDING',]):
        parent_task.status = 'PENDING'
    else:
        if not Task.objects.filter(parent=parent_task).exclude(status='SUCCESS'):
            parent_task.status = 'SUCCESS'
    parent_task.save()


def update_dependent_tasks(id):
    task = Task.objects.get(id=id)

    if task.status == 'SUCCESS':
        # First handle direct task dependencies
        parent_tasks_ids = []
        for dependent_task in Task.objects.filter(dependent_on_task=task, status='PENDING'):
            dependent_task.target_start_datetime = timezone.now()
            dependent_task.target_end_datetime = timezone.now() + timezone.timedelta(days=dependent_task.duration)
            dependent_task.status = 'STARTED'
            dependent_task.save()
            if dependent_task.parent:
                parent_tasks_ids.append(dependent_task.parent.id)
            email_task_owners(dependent_task.id)
        if task.parent:
            update_parent_task_status(task.parent.id)

        parent_tasks_ids = list(set(parent_tasks_ids))
        for task_id in parent_tasks_ids:
            update_parent_task_status(task_id)

        # Then handle section dependencies
        parent_tasks_ids = []
        if not Task.objects.filter(section=task.section).exclude(status='SUCCESS'):
            for dependent_task in Task.objects.filter(dependent_on_section=task.section, dependent_on_task__isnull=True, status='PENDING'):
                dependent_task.target_start_datetime = timezone.now()
                dependent_task.target_end_datetime = timezone.now() + timezone.timedelta(days=dependent_task.duration)
                dependent_task.status = 'STARTED'
                dependent_task.save()
                if dependent_task.parent:
                    parent_tasks_ids.append(dependent_task.parent.id)
                email_task_owners(dependent_task.id)

            parent_tasks_ids = list(set(parent_tasks_ids))
            for task_id in parent_tasks_ids:
                update_parent_task_status(task_id)





class TaskApprovalSerializer(serializers.Serializer):
    task = serializers.IntegerField()
    notes = serializers.CharField()
    action = serializers.ChoiceField(choices=['approve', 'reject'])

    def validate_task(self, value):
        try:
            task = Task.objects.get(id=value)
            teams = Team.objects.filter(teammember__member =self.context['request'].user)
            print(task.assigned_to_user)
            print(self.context['request'].user)
            if task.status != 'STARTED':
                raise serializers.ValidationError('Task must be in Pending state to Approve/Reject task'.format(value))
            elif task.assigned_to_team and task.assigned_to_team not in teams:
                raise serializers.ValidationError('This task is not assign to you or your team to Approve/Reject'.format(value))
            elif task.assigned_to_user and task.assigned_to_user != self.context['request'].user:
                raise serializers.ValidationError('This task is not assign to you to Approve/Reject'.format(value))


        except Task.DoesNotExist:
            raise serializers.ValidationError('Task with id {} does not exist'.format(value))
        return value


    def save(self, **kwargs):
        task = Task.objects.get(id=self.data.get('task'))
        task.notes = self.data.get('notes')
        task.executed_by = self.context['request'].user
        task.modified_by = self.context['request'].user
        task.executed_timestamp = timezone.now()

        action = self.data.get('action')

        if action == 'approve':
            task.approval_status = 'A'
            task.status = 'SUCCESS'
        elif action == 'reject':
            # TODO : Change status to Rejected rather than failure
            task.approval_status = 'R'
            task.status = 'FAILURE'
        task.save()

        update_dependent_tasks(task.id)


class TaskCompleteSerializer(serializers.Serializer):
    task = serializers.IntegerField()
    notes = serializers.CharField()

    def validate_task(self, value):
        try:
            task = Task.objects.get(id=value)
            teams = Team.objects.filter(teammember__member=self.context['request'].user)
            if task.status != 'STARTED':
                raise serializers.ValidationError('Task must be in In Progress to Approve/Reject task'.format(value))
            elif task.assigned_to_team and task.assigned_to_team not in teams:
                raise serializers.ValidationError('This task is not assign to you or your team to Complete'.format(value))
            elif task.assigned_to_user and task.assigned_to_user != self.context['request'].user  :
                raise serializers.ValidationError('This task is not assign to you to Complete'.format(value))

        except Task.DoesNotExist:
            raise serializers.ValidationError('Task with id {} does not exist'.format(value))
        return value

    def save(self, **kwargs):
        task = Task.objects.get(id=self.data.get('task'))
        task.notes = self.data.get('notes')
        task.executed_by = self.context['request'].user
        task.modified_by = self.context['request'].user
        task.executed_timestamp = timezone.now()
        task.status = 'SUCCESS'
        task.save()

        update_dependent_tasks(task.id)


class TaskNoteSerializer(serializers.Serializer):
    task = serializers.IntegerField()
    notes = serializers.CharField()

    def validate_task(self, value):
        try:
            task = Task.objects.get(id=value)
            if task.status != 'STARTED':
                raise serializers.ValidationError('Task must be in Pending state to Approve/Reject task'.format(value))
        except Task.DoesNotExist:
            raise serializers.ValidationError('Task with id {} does not exist'.format(value))
        return value

    def save(self, **kwargs):
        task = Task.objects.get(id=self.data.get('task'))
        Note.objects.create(
            task=task, notes=self.data.get('notes'),
            created_by=self.context['request'].user, modified_by=self.context['request'].user
        )


class TaskAttachmentSerializer(serializers.Serializer):
    task = serializers.IntegerField()
    name = serializers.CharField(max_length=100)
    description = serializers.CharField(max_length=2000)
    file = serializers.FileField()

    def validate_task(self, value):
        try:
            task = Task.objects.get(id=value)
            if task.status != 'STARTED':
                raise serializers.ValidationError('Task must be in Pending state to Approve/Reject task'.format(value))
        except Task.DoesNotExist:
            raise serializers.ValidationError('Task with id {} does not exist'.format(value))
        return value

    def save(self, **kwargs):
        print('HERE')
        print(self.data)
        print(dir(self))
        print(kwargs)


class TaskChoiceSerializer(serializers.ModelSerializer):
    friendly_name = serializers.SerializerMethodField()

    def get_friendly_name(self, obj):
        if obj.parent:
            return '{} -> {}'.format(obj.parent.name, obj.name)
        else:
            return obj.name


    class Meta:
        model = Task
        fields = ('id', 'name', 'friendly_name')
