# Generated by Django 2.1 on 2018-09-06 04:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0003_auto_20180903_0714'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='order',
            field=models.PositiveIntegerField(default=0),
        ),
    ]
